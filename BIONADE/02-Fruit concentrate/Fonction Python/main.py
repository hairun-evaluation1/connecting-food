import os
import pandas as pd
from model.modelclass import Model_class
from utils.constant import (
    INPUT_FILE_PATH,
    OUTPUT_FILE_PATH,
    NULL,
    STEP,
    ACTOR,
    SITE,
    CATEGORY,
    ORIGIN_FRUIT,
)

SOURCE_PATH = os.path.dirname(os.path.abspath(__file__))


def format_csv(source_file_path, output_file_path):
    """Function used to transform an input csv into a formatted one"""

    df = pd.read_csv(source_file_path, delimiter=";", encoding="utf-8")
    column_number = df.shape[1]

    row_list = []

    # Check if column number of the input data exceed 9
    if column_number >= 9:
        print("Formatting...")
        # Get input data line by line

        for index, row in df.iterrows():
            # Dict model
            factory_dict = {
                "category": CATEGORY,
                "step": STEP,
                "level": NULL,
                "capture_start": row.get("Date of fabrication"),
                "capture_end": NULL,
                "capture_uid": NULL,
                "sender_actor_number": ACTOR,
                "sender_location_number": SITE,
                "sender_asset_number": NULL,
                "sender_track_id": row.get("Concentrate batch number"),
                "sender_sku": NULL,
                "sender_batch_number": NULL,
                "sender_model_number": NULL,
                "sender_production_date": row.get("Date of fabrication"),
                "sender_product": row.get("Product code out"),
                "sender_quantity": row.get("Quantity out (tons)"),
                "sender_unit": NULL,
                "receiver_actor_number": ACTOR,
                "receiver_location_number": SITE,
                "receiver_asset_number": NULL,
                "receiver_track_id": row.get("Concentrate batch number"),
                "receiver_sku": NULL,
                "receiver_batch_number": NULL,
                "receiver_model_number": NULL,
                "receiver_production_date": NULL,
                "receiver_product": NULL,
                "receiver_quantity": NULL,
                "receiver_unit": NULL,
                "tags": NULL,
                "payload__year_of_production": row.get("Year of production"),
                "payload__origine_of_the_fruit": ORIGIN_FRUIT,
                "payload__concentrate_batch_number": row.get(
                    "Concentrate batch number"
                ),
                "payload__quantity_in": row.get("Quantity in (tons)"),
            }

            row_list.append(Model_class(**factory_dict).__dict__)

        # Dataframe transformation
        result = pd.DataFrame(row_list)

        # Dataframe into CSV
        result.to_csv(output_file_path, index=False, sep=",", encoding="utf-8")
        print("Finished")
    else:
        print("There is not enough column")


if __name__ == "__main__":
    format_csv(
        source_file_path=SOURCE_PATH + INPUT_FILE_PATH,
        output_file_path=SOURCE_PATH + OUTPUT_FILE_PATH,
    )
