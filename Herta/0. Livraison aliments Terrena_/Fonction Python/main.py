import os
import pandas as pd
from model.food_delivery import FoodDelivery
from utils.constant import (
    INPUT_FILE_PATH,
    OUTPUT_FILE_PATH,
    NULL,
    STEP,
    ACTOR_SENDER,
    CATEGORY,
)

SOURCE_PATH = os.path.dirname(os.path.abspath(__file__))


def format_csv(source_file_path, output_file_path):
    """Function used to transform an input csv into a formatted one"""

    df = pd.read_csv(source_file_path, delimiter=";")
    column_number = df.shape[1]
    row_list = []
    # Check if column number of the input data exceed 12
    if column_number >= 12:
        print("Formatting...")

        # Get input data line by line
        for index, row in df.iterrows():
            quantity_str = row.get("quantite")
            quantity = (
                "0" + quantity_str if quantity_str[0] == "," else quantity_str
            ).replace(",", ".")
            # Dict model
            food_delivery_dict = {
                "category": CATEGORY,
                "step": STEP,
                "level": NULL,
                "capture_start": row.get("date_expe")+("20"),
                "capture_end": NULL,
                "capture_uid": NULL,
                "sender_actor_number": ACTOR_SENDER,
                "sender_location_number": NULL,
                "sender_asset_number": NULL,
                "sender_track_id": NULL,
                "sender_sku": NULL,
                "sender_batch_number": NULL,
                "sender_model_number": NULL,
                "sender_production_date": row.get("date_expe"),
                "sender_product": row.get("code_produit"),
                "sender_quantity": quantity,
                "sender_unit": row.get("unite"),
                "receiver_actor_number": row.get("siret_livre"),
                "receiver_location_number": row.get("code_site"),
                "receiver_asset_number": NULL,
                "receiver_track_id": row.get("lot"),
                "receiver_sku": NULL,
                "receiver_batch_number": NULL,
                "receiver_model_number": NULL,
                "receiver_production_date": NULL,
                "receiver_product": NULL,
                "receiver_quantity": NULL,
                "receiver_unit": NULL,
                "tags": NULL,
                "payload__lot_expédié": row.get("lot"),
                "payload__siret_facturé": row.get("siret_facture"),
                "payload__libellé_produit": row.get("lib_produit"),
                "payload__unité": row.get("unite"),
                "payload__dtcharge": row.get("dtcharge"),
            }

            row_list.append(FoodDelivery(**food_delivery_dict).__dict__)

        # Dataframe transformation
        result = pd.DataFrame(row_list)
        # Dataframe into CSV
        result.to_csv(output_file_path, index=False, sep=",", encoding="utf-8")
        print("Finished")
    else:
        print("There is not enough column")


if __name__ == "__main__":

    format_csv(
        source_file_path=SOURCE_PATH + INPUT_FILE_PATH,
        output_file_path=SOURCE_PATH + OUTPUT_FILE_PATH,
    )
