import os
import json
import pandas as pd
from model.modelclass import Model_class
from utils.constant import INPUT_FILE_PATH, OUTPUT_FILE_PATH, NULL, STEP, CATEGORY

SOURCE_PATH = os.path.dirname(os.path.abspath(__file__))


def format_csv(source_file_path, output_file_path):
    """Function used to transform an input csv into a formatted one"""

    df = pd.read_csv(source_file_path, delimiter=";", decimal=".", encoding="utf-8")

    column_number = df.shape[1]

    row_list = []

    # Check if column number of the input data exceed 10
    if column_number >= 10:
        print("Formatting...")

        # Get input data line by line
        for index, row in df.iterrows():
            lots_livraison = str(row.get("ref_lot"))
            actor_sender = row.get("ref_actor_producer")
            sender_actor_number = (
                "38156184400600" if actor_sender == "38156184400022" else actor_sender
            )

            # Apply transformation
            for lot_livraison in lots_livraison.split("\\"):
                # Dict model
                factory_dict = {
                    "category": CATEGORY,
                    "step": STEP,
                    "level": NULL,
                    "capture_start": row.get("delivery_at"),
                    "capture_end": NULL,
                    "capture_uid": NULL,
                    "sender_actor_number": sender_actor_number,
                    "sender_location_number": row.get("ref_site"),
                    "sender_asset_number": NULL,
                    "sender_track_id": NULL,
                    "sender_sku": NULL,
                    "sender_batch_number": NULL,
                    "sender_model_number": NULL,
                    "sender_production_date": row.get("delivery_at"),
                    "sender_product": row.get("ref_product"),
                    "sender_quantity": row.get("quantity"),
                    "sender_unit": row.get("unit"),
                    "receiver_actor_number": row.get("ref_actor_receipt"),
                    "receiver_location_number": NULL,
                    "receiver_asset_number": NULL,
                    "receiver_track_id": row.get("ref_lot_d"),
                    "receiver_sku": NULL,
                    "receiver_batch_number": NULL,
                    "receiver_model_number": NULL,
                    "receiver_production_date": NULL,
                    "receiver_product": NULL,
                    "receiver_quantity": NULL,
                    "receiver_unit": NULL,
                    "tags": NULL,
                    "payload__numero_bande_associé": row.get("ref_lot_d"),
                    "payload__lot_aliments_livré": lot_livraison,
                }

                row_list.append(Model_class(**factory_dict).__dict__)

        # Dataframe transformation
        result = pd.DataFrame(row_list)
        # Dataframe into CSV
        result.to_csv(output_file_path, index=False, sep=",", encoding="utf-8")
        print("Finished")
    else:
        print("There is not enough column")


if __name__ == "__main__":
    format_csv(
        source_file_path=SOURCE_PATH + INPUT_FILE_PATH,
        output_file_path=SOURCE_PATH + OUTPUT_FILE_PATH,
    )
