from utils.utils import generate_output_filename

NULL = None
CATEGORY = "Séchage"
STEP = "processing"
ACTOR = "38881872600100"

INPUT_FILE_PATH = "/data/traça lot 8618040.csv"
OUTPUT_FILE_PATH = "/output/Output - " + generate_output_filename()
