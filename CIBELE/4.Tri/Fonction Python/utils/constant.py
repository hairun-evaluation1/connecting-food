from utils.utils import generate_output_filename

NULL = None
CATEGORY = "Tri"
STEP = "inspecting"
ACTOR = "39198157800035"
SITE = "2930"
INPUT_FILE_PATH = "/data/20220712 TRI.csv"
OUTPUT_FILE_PATH = "/output/Output - " + generate_output_filename()
