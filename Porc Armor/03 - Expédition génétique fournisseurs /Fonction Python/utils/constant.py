from utils.utils import generate_output_filename

NULL = None
CATEGORY = "Expédition génétique fournisseurs"
STEP = "loading"
INPUT_FILE_PATH = "/data/Extraction_BDPorc_modifiée_v400_t17yODT.csv"
OUTPUT_FILE_PATH = "/output/Output - " + generate_output_filename()
