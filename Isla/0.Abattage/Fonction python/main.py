import os
import pandas as pd
from model.modelclass import ModelClass
from utils.constant import (
    STEP,
    INPUT_FILE_PATH,
    OUTPUT_FILE_PATH,
    NULL,
    ACTOR_RECEIVER,
    SITE_RECEIVER,
    CATEGORY
)

SOURCE_PATH = os.path.dirname(os.path.abspath(__file__))


def format_csv(source_file_path, output_file_path):
    """Function used to transform an input csv into a formatted one"""

    df = pd.read_csv(source_file_path, delimiter=";")
    row_list = []
    print("Formatting...")

    # Get input data line by line

    for index, row in df.iterrows():
        factory_dict = {
            "category": CATEGORY,
            "step": STEP,
            "level": NULL,
            "capture_start": row.get("Date de réception"),
            "capture_end": NULL,
            "capture_uid": NULL,
            "sender_actor_number": row.get("Code fournisseur"),
            "sender_location_number": row.get("eleveur lot origine 1"),
            "sender_asset_number": NULL,
            "sender_track_id": row.get("Lot entrant"),
            "sender_sku": NULL,
            "sender_batch_number": NULL,
            "sender_model_number": NULL,
            "sender_production_date": row.get("Date de réception"),
            "sender_product": row.get("Code interne produit"),
            "sender_quantity": row.get("Quantités"),
            "sender_unit": row.get("Unité"),
            "receiver_actor_number": ACTOR_RECEIVER,
            "receiver_location_number": SITE_RECEIVER,
            "receiver_asset_number": NULL,
            "receiver_track_id": row.get("Lot sortant"),
            "receiver_sku": NULL,
            "receiver_batch_number": NULL,
            "receiver_model_number": NULL,
            "receiver_production_date": NULL,
            "receiver_product": NULL,
            "receiver_quantity": NULL,
            "receiver_unit": NULL,
            "tags": NULL,
            "payload__date_d_abattage": row.get("Date d'abattage"),
            "payload__quantite": row.get("Quantités"),
            "payload__unité": row.get("Unité"),
            "payload__nom_éleveur": row.get("Nom éleveur"),
            "payload__certification_halal": row.get("Certification Halal"),
            "payload__date_de_l_expedition": row.get("Date de l'expédition"),
            "payload__lot_sortant": row.get("Lot sortant"),
        }
        row_list.append(ModelClass(**factory_dict).__dict__)

    # Dataframe transformation
    result = pd.DataFrame(row_list)

    # Dataframe into CSV
    result.to_csv(output_file_path, index=False, sep=",")
    print("Finished")


if __name__ == "__main__":
    format_csv(
        source_file_path=SOURCE_PATH + INPUT_FILE_PATH,
        output_file_path=SOURCE_PATH + OUTPUT_FILE_PATH,
    )
