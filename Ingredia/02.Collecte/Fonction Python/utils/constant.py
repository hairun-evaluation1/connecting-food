from utils.utils import generate_output_filename

NULL = None
CATEGORY = "Collecte"
STEP = "receiving"

INPUT_FILE_PATH = (
    "/data/connectingfood_ingredia_vialacta_RECEIVE_03_04_2023_08_46_v1.csv"
)
OUTPUT_FILE_PATH = "/output/Output - " + generate_output_filename()
