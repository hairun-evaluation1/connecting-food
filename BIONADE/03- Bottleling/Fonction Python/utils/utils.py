import datetime


def generate_output_filename():
    now = datetime.datetime.now()
    return (
        "Holunder Abfüllung für FR_QP1NWaE - "
        + now.strftime("%d_%m_%Y %H_%M_%S")
        + ".csv"
    )
