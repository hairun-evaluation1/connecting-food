from utils.utils import generate_output_filename

NULL = None
CATEGORY = "Tri"
STEP = "processing"
INPUT_FILE_PATH = "/data/6.tri_20200324_114236_LastweekMars.csv"
OUTPUT_FILE_PATH = "/output/Output - " + generate_output_filename()
