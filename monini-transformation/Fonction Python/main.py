import pandas as pd
import datetime
import math
from utils.constant import INPUT_FILE_PATH


def transform_csv(source_file_path, output_path):

    now = datetime.datetime.now()

    # Charger le fichier CSV dans un DataFrame
    df = pd.read_csv(source_file_path)

    # Créer une nouvelle colonne 'reference_copy' pour stocker les références copiées
    df['reference_copy'] = ''

    # Itérer sur les lignes du DataFrame
    for index, row in df.iterrows():
        if row['level'] == 2 or row['level'] == 3:
            # Recherche de la ligne dont le provider n'est pas vide
            parent_index = index - 1
            while parent_index >= 0 and pd.isna(df.at[parent_index, 'name']):
                parent_index -= 1
            
            # Copier la référence de la ligne parente dans la colonne 'reference_copy'
            if parent_index >= 0:
                df.at[index, 'reference_copy'] = df.at[parent_index, 'reference']

    # recopier la colonne "reference_copy" dans la colonne 'reference'
    df['reference'] = df.apply(lambda row: row['reference_copy'] if row['reference_copy'] != "" else row['reference'], axis=1)
    df['provider'] = df.apply(lambda row : str(int(row['provider'])) if not math.isnan(row['provider']) else row['provider'], axis=1)

    # supprimer la colonne "reference_copy"
    df.drop(columns=['reference_copy'], inplace=True)
    
    df.to_csv(f'{output_path}output_{now}.csv', index=False)

    print("Finished")

if __name__ == "__main__":
    
    transform_csv(
        # A changer selon le nom du fichier à transformer
        source_file_path=INPUT_FILE_PATH,

        # Dossier de sortie
        output_path="output/"
    )