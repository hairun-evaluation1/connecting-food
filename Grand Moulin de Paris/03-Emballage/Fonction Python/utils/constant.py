from utils.utils import generate_output_filename

NULL = None
CATEGORY = "Emballage"
STEP = "packing"
INPUT_FILE_PATH = "/data/20230402_200_FrancineBIO.csv"
OUTPUT_FILE_PATH = "/output/Output - " + generate_output_filename()
