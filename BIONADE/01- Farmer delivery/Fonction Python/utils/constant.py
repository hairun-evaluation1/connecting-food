from utils.utils import generate_output_filename

NULL = None
CATEGORY = "Farmer delivery"
STEP = "receiving"
SITE_SENDER = "reference"
SITE_RECEIVER = "Mainfrucht"
ACTOR = "302005341"


INPUT_FILE_PATH = "/data/1_farmer_delivery.csv"
OUTPUT_FILE_PATH = "/output/Output - " + generate_output_filename()
