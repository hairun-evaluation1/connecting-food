import datetime


def generate_output_filename():
    now = datetime.datetime.now()
    return "traça lot 8618040 - " + now.strftime("%d_%m_%Y %H_%M_%S") + ".csv"
