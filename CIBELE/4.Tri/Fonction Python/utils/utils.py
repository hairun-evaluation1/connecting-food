import datetime


def generate_output_filename():
    now = datetime.datetime.now()
    return now.strftime("%Y%m%d") + "_TRI.csv"


def add_tri(x):
    return str(x) + "/tri"
