import os
import pandas as pd
from model.transformation import Transformation
from utils.constant import (
    INPUT_FILE_PATH,
    OUTPUT_FILE_PATH,
    NULL,
    STEP,
    CATEGORY,
)

SOURCE_PATH = os.path.dirname(os.path.abspath(__file__))


def format_csv(source_file_path, output_file_path):
    """Function used to transform an input csv into a formatted one"""

    df = pd.read_csv(
        source_file_path,
        delimiter=";",
        encoding="iso-8859-1",
        skiprows=[1],
    )
    column_number = df.shape[1]
    row_list = []

    # Check if column number of the input data exceed 16
    if column_number >= 16:
        print("Formatting...")

        # Get input data line by line
        for index, row in df.iterrows():

            # Dict model
            transformation_dict = {
                "category": CATEGORY,
                "step": STEP,
                "level": NULL,
                "capture_start": row.get("product_at"),
                "capture_end": NULL,
                "capture_uid": NULL,
                "sender_actor_number": row.get("ref_actor_producer"),
                "sender_location_number": NULL,
                "sender_asset_number": row.get("code_stock_tool"),
                "sender_track_id": row.get("ref_lot"),
                "sender_sku": NULL,
                "sender_batch_number": NULL,
                "sender_model_number": NULL,
                "sender_production_date": row.get("product_at"),
                "sender_product": row.get("ref_product"),
                "sender_quantity": row.get("quantity"),
                "sender_unit": row.get("unit"),
                "receiver_actor_number": row.get("ref_actor_producer"),
                "receiver_location_number": NULL,
                "receiver_asset_number": row.get("code_stock_tool.1"),
                "receiver_track_id": row.get("ref_lot_d"),
                "receiver_sku": NULL,
                "receiver_batch_number": NULL,
                "receiver_model_number": NULL,
                "receiver_production_date": NULL,
                "receiver_product": NULL,
                "receiver_quantity": NULL,
                "receiver_unit": NULL,
                "tags": NULL,
                "payload__lot_entrant": row.get("ref_lot"),
                "payload__code_interne_produit_entrant": row.get("code"),
                "payload__lot_sortant": row.get("ref_lot_d"),
                "payload__code_interne_outil_production": row.get(
                    "ref_prod_tool"
                ),
                "payload__DDM": row.get("ddm"),
                "payload__heure": row.get("hour"),
            }

            row_list.append(Transformation(**transformation_dict).__dict__)

        # Dataframe transformation
        result = pd.DataFrame(row_list)
        # Dataframe into CSV
        result.to_csv(output_file_path, index=False, sep=",")
        print("Finished")
    else:
        print("There is not enough column")


if __name__ == "__main__":

    format_csv(
        source_file_path=SOURCE_PATH + INPUT_FILE_PATH,
        output_file_path=SOURCE_PATH + OUTPUT_FILE_PATH,
    )
