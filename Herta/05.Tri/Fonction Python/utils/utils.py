import datetime


def generate_output_filename():
    now = datetime.datetime.now()
    return "tri_" + now.strftime("%d_%m_%Y") + ".csv"
