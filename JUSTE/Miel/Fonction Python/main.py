import os
import pandas as pd
from model.honey import Honey
from utils.constant import (
    INPUT_FILE_PATH,
    OUTPUT_FILE_PATH,
    NULL,
    STEP,
    PRODUCT,
    ACTOR_RECEIVER,
    CATEGORY,
)

SOURCE_PATH = os.path.dirname(os.path.abspath(__file__))


def format_csv(source_file_path, output_file_path):
    """Function used to transform an input csv into a formatted one"""

    df = pd.read_csv(
        source_file_path,
        delimiter=";",
        encoding="iso-8859-1",
    )
    column_number = df.shape[1]
    row_list = []

    # Check if column number of the input data exceed 5
    if column_number >= 5:
        print("Formatting...")

        # Get input data line by line
        for index, row in df.iterrows():

            quantity = str(row.get("Production en kg")).replace(",", ".")

            # Dict model
            honey_dict = {
                "category": CATEGORY,
                "step": STEP,
                "level": NULL,
                "capture_start": row.get("Mois"),
                "capture_end": NULL,
                "capture_uid": NULL,
                "sender_actor_number": row.get("Apiculteur"),
                "sender_location_number": row.get("Ruchers"),
                "sender_asset_number": NULL,
                "sender_track_id": NULL,
                "sender_sku": NULL,
                "sender_batch_number": NULL,
                "sender_model_number": NULL,
                "sender_production_date": row.get("Mois"),
                "sender_product": PRODUCT,
                "sender_quantity": quantity,
                "sender_unit": NULL,
                "receiver_actor_number": ACTOR_RECEIVER,
                "receiver_location_number": NULL,
                "receiver_asset_number": NULL,
                "receiver_track_id": row.get("N° de lot correspondant"),
                "receiver_sku": NULL,
                "receiver_batch_number": NULL,
                "receiver_model_number": NULL,
                "receiver_production_date": NULL,
                "receiver_product": NULL,
                "receiver_quantity": NULL,
                "receiver_unit": NULL,
                "tags": NULL,
                "payload__n°_de_lot_correspondant": row.get(
                    "N° de lot correspondant"
                ),
            }

            row_list.append(Honey(**honey_dict).__dict__)

        # Dataframe transformation
        result = pd.DataFrame(row_list)
        # Dataframe into CSV
        result.to_csv(output_file_path, index=False, sep=",")
        print("Finished")
    else:
        print("There is not enough column")


if __name__ == "__main__":

    format_csv(
        source_file_path=SOURCE_PATH + INPUT_FILE_PATH,
        output_file_path=SOURCE_PATH + OUTPUT_FILE_PATH,
    )
