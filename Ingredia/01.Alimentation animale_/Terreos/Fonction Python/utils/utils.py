import datetime


def generate_output_filename():
    now = datetime.datetime.now()
    return "FLUX_ALIMENTS_TEREOS_PH_" + now.strftime("%Y_%m_%d") + ".csv"
