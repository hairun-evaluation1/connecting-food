from utils.utils import generate_output_filename

NULL = None
CATEGORY = "reception usine"
STEP = "receiving"
LEVEL = 2
RECEIVER_ACTOR_NUMBER = "53844948900029"
RECEIVER_UNIT = "kg"
PRODUCTS = [
    "AMIDON DE MAIS NATIF sac de 25kg",
    "SEL NITRITE 0,85%",
    "FIBRE DE POIS LI01016 SOUSSANA",
]


INPUT_FILE_PATH = "/data/BlockchainRsc - 18_05_22 21_15_55.csv"
OUTPUT_FILE_PATH = "/output/Output - " + generate_output_filename()
