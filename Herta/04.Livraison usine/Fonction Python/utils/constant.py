from utils.utils import generate_output_filename

NULL = None
CATEGORY = "Livraison usine"
STEP = "receiving"
INPUT_FILE_PATH = (
    "/data/04reception_20201030_223022.01d6af06331c0ff2238940a8f02f6924.CSV"
)
OUTPUT_FILE_PATH = "/output/Output - " + generate_output_filename()
