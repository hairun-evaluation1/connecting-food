import os
import pandas as pd
from model.bottleling import Bottleling
from utils.constant import (
    INPUT_FILE_PATH,
    OUTPUT_FILE_PATH,
    NULL,
    STEP,
    CATEGORY,
    ACTOR,
)

SOURCE_PATH = os.path.dirname(os.path.abspath(__file__))


def format_csv(source_file_path, output_file_path):
    """Function used to transform an input csv into a formatted one"""

    df = pd.read_csv(
        source_file_path, delimiter=";", encoding="iso-8859-1", skiprows=[0]
    )
    column_number = df.shape[1]
    row_list = []

    # Check if column number of the input data exceed 11
    if column_number >= 11:
        print("Formatting...")

        # Get input data line by line
        for index, row in df.iterrows():

            # Dict model
            bottleling_dict = {
                "category": CATEGORY,
                "step": STEP,
                "level": NULL,
                "capture_start": row.get("Date of bottling"),
                "capture_end": NULL,
                "capture_uid": NULL,
                "sender_actor_number": ACTOR,
                "sender_location_number": row.get("Plant code"),
                "sender_asset_number": NULL,
                "sender_track_id": row.get("Batch number in"),
                "sender_sku": NULL,
                "sender_batch_number": NULL,
                "sender_model_number": NULL,
                "sender_production_date": row.get("Date of bottling"),
                "sender_product": row.get("Code product out"),
                "sender_quantity": row.get("Quantity bottled").replace(
                    ",", "."
                ),
                "sender_unit": row.get("Unit"),
                "receiver_actor_number": ACTOR,
                "receiver_location_number": row.get("Plant code"),
                "receiver_asset_number": NULL,
                "receiver_track_id": row.get("Batch number out"),
                "receiver_sku": NULL,
                "receiver_batch_number": NULL,
                "receiver_model_number": NULL,
                "receiver_production_date": NULL,
                "receiver_product": NULL,
                "receiver_quantity": NULL,
                "receiver_unit": NULL,
                "tags": NULL,
                "payload__code_product_in": row.get("Code product in (sirup)"),
                "payload__sirup_batch_number_in": row.get("Batch number in"),
                "payload__final_batch_number": row.get("Batch number out"),
                "payload__number_of_bottles_filled": row.get(
                    "Number of bottles filled"
                ),
            }

            row_list.append(Bottleling(**bottleling_dict).__dict__)

        # Dataframe transformation
        result = pd.DataFrame(row_list)
        # Dataframe into CSV
        result.to_csv(output_file_path, index=False, sep=",")
        print("Finished")
    else:
        print("There is not enough column")


if __name__ == "__main__":

    format_csv(
        source_file_path=SOURCE_PATH + INPUT_FILE_PATH,
        output_file_path=SOURCE_PATH + OUTPUT_FILE_PATH,
    )
