import datetime


def generate_output_filename():
    now = datetime.datetime.now()
    return (
        "Extraction_BDPorc_modifiée_v400_t17yODT_" + now.strftime("%d_%m_%Y") + ".csv"
    )
