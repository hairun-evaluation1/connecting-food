import datetime


def generate_output_filename():
    now = datetime.datetime.now()
    return "03HERTA_BL_" + now.strftime("%d_%m_%Y") + ".csv"
