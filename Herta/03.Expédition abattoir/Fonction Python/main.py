import os, re
import pandas as pd
from model.modelclass import Model_class
from utils.constant import INPUT_FILE_PATH, OUTPUT_FILE_PATH, NULL, STEP, CATEGORY

SOURCE_PATH = os.path.dirname(os.path.abspath(__file__))


def format_csv(source_file_path, output_file_path):
    """Function used to transform an input csv into a formatted one"""

    df = pd.read_csv(source_file_path, delimiter=";")

    column_number = df.shape[1]

    row_list = []

    # Check if column number of the input data exceed 10
    if column_number >= 10:
        print("Formatting...")

        # Get input data line by line
        for index, row in df.iterrows():
            quantite_str = str(re.sub(r"0+([1-9][0-9]*)", r"\1", row.get("quantite")))
            quantity = quantite_str.replace(",", ".")

            # Dict model
            factory_dict = {
                "category": CATEGORY,
                "step": STEP,
                "level": NULL,
                "capture_start": row.get("date_expedition"),
                "capture_end": NULL,
                "capture_uid": NULL,
                "sender_actor_number": row.get("siret_abattoir"),
                "sender_location_number": row.get("code_site_abattoir"),
                "sender_asset_number": NULL,
                "sender_track_id": row.get("lot_livre"),
                "sender_sku": NULL,
                "sender_batch_number": NULL,
                "sender_model_number": NULL,
                "sender_production_date": row.get("date_expedition"),
                "sender_product": row.get("code_produit_livre"),
                "sender_quantity": quantity,
                "sender_unit": row.get("unite"),
                "receiver_actor_number": row.get("code_client_livre"),
                "receiver_location_number": NULL,
                "receiver_asset_number": NULL,
                "receiver_track_id": row.get("lot_livre"),
                "receiver_sku": NULL,
                "receiver_batch_number": NULL,
                "receiver_model_number": NULL,
                "receiver_production_date": NULL,
                "receiver_product": NULL,
                "receiver_quantity": NULL,
                "receiver_unit": NULL,
                "tags": NULL,
                "payload__lot_livré": row.get("lot_livre"),
                "payload__unité": row.get("unite"),
            }

            row_list.append(Model_class(**factory_dict).__dict__)

        # Dataframe transformation
        result = pd.DataFrame(row_list)
        # Dataframe into CSV
        result.to_csv(output_file_path, index=False, sep=",", encoding="utf-8")
        print("Finished")
    else:
        print("There is not enough column")


if __name__ == "__main__":
    format_csv(
        source_file_path=SOURCE_PATH + INPUT_FILE_PATH,
        output_file_path=SOURCE_PATH + OUTPUT_FILE_PATH,
    )
