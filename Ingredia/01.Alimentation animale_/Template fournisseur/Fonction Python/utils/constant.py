from utils.utils import generate_output_filename

NULL = None
ACTOR_RECEIVER = "44867070300017"
CATEGORY = "Alimentation animale template fournisseur"
STEP = "receiving"
INPUT_FILE_PATH = "/data/PF_FLUX_ALIMENTS_01_03_2023_11_48.csv"
OUTPUT_FILE_PATH = "/output/Output - " + generate_output_filename()
