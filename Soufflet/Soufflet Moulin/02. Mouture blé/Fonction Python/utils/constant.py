from utils.utils import generate_output_filename

NULL = None
INPUT_FILE_PATH = "/data/20230322 - M02CB - EXPORT - OF MOUTURES.csv"
OUTPUT_FILE_PATH = "/output/Output - " + generate_output_filename()
CATEGORY = "Mouture blé"
STEP = "processing"
