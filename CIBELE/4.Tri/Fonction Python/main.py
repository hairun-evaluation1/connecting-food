import os
import pandas as pd
from model.modelclass import Model_class
from utils.constant import INPUT_FILE_PATH, OUTPUT_FILE_PATH, NULL, STEP, ACTOR, SITE , CATEGORY
from utils.utils import add_tri

SOURCE_PATH = os.path.dirname(os.path.abspath(__file__))


def format_csv(source_file_path, output_file_path):
    """Function used to transform an input csv into a formatted one"""

    df = pd.read_csv(
        source_file_path, delimiter=";", decimal=".", encoding="iso-8859-1"
    )
    df["N\u00b0 container Tri"] = df["N\u00b0 container Tri"].apply(add_tri)
    column_number = df.shape[1]

    row_list = []

    # Check if column number of the input data exceed 10
    if column_number >= 10:
        print("Formatting...")
        # Get input data line by line

        for index, row in df.iterrows():
            container_tri = row.get("N\u00b0 container Tri")

            for container in container_tri.split("/"):
                # Dict model
                factory_dict = {
                    "category": CATEGORY,
                    "step": STEP,
                    "level": NULL,
                    "capture_start": row.get("Date Tri"),
                    "capture_end": NULL,
                    "capture_uid": NULL,
                    "sender_actor_number": ACTOR,
                    "sender_location_number": SITE,
                    "sender_asset_number": row.get("Container Origine"),
                    "sender_track_id": row.get("Container Origine"),
                    "sender_sku": NULL,
                    "sender_batch_number": NULL,
                    "sender_model_number": NULL,
                    "sender_production_date": row.get("Date Tri"),
                    "sender_product": row.get("Code produit tri\u00e9"),
                    "sender_quantity": row.get("Poids Net"),
                    "sender_unit": NULL,
                    "receiver_actor_number": ACTOR,
                    "receiver_location_number": SITE,
                    "receiver_asset_number": container,
                    "receiver_track_id": container,
                    "receiver_sku": NULL,
                    "receiver_batch_number": NULL,
                    "receiver_model_number": NULL,
                    "receiver_production_date": NULL,
                    "receiver_product": NULL,
                    "receiver_quantity": NULL,
                    "receiver_unit": NULL,
                    "tags": NULL,
                    "payload__numero_tri": row.get("N\u00b0 Tri"),
                    "payload__origine": row.get("Origine L / 503 / 002"),
                    "payload__poids_brut": row.get("Poids Brut"),
                    "payload__numero_livraison": row.get("N\u00b0 Liv"),
                }

                row_list.append(Model_class(**factory_dict).__dict__)

        # Dataframe transformation
        result = pd.DataFrame(row_list)

        # Dataframe into CSV
        result.to_csv(output_file_path, index=False, sep=",", encoding="utf-8")
        print("Finished")
    else:
        print("There is not enough column")


if __name__ == "__main__":
    format_csv(
        source_file_path=SOURCE_PATH + INPUT_FILE_PATH,
        output_file_path=SOURCE_PATH + OUTPUT_FILE_PATH,
    )
