from utils.utils import generate_output_filename

NULL = None
SITE_RECEIVER = "SERETRAM"
PRODUCT = "Boite ma\u00efs"
QUANTITY = "1"
ACTOR_RECEIVER = "44867070300017"
CATEGORY = "Production agricole"
STEP = "consigning"
INPUT_FILE_PATH = "/data/Trac_Agro_2022_10_03.csv"
OUTPUT_FILE_PATH = "/output/Output - " + generate_output_filename()
