from utils.utils import generate_output_filename

NULL = None
CATEGORY = "Conditionnement blé"
STEP = "packing"
INPUT_FILE_PATH = (
    "/data/20230322 - M02CB - EXPORT - OF MOUTURES - CONDITIONNEMENTS 1KG.csv"
)
OUTPUT_FILE_PATH = "/output/Output - " + generate_output_filename()
