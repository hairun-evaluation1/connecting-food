import datetime


def generate_output_filename():
    now = datetime.datetime.now()
    return "PF_FLUX_ALIMENTS_" + now.strftime("%d_%m_%Y") + ".csv"
