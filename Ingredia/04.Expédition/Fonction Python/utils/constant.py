from utils.utils import generate_output_filename

NULL = None
CATEGORY = "Expédition"
STEP = "consigning"

INPUT_FILE_PATH = (
    "/data/connectingfood_ingredia_vialacta_DELIVERY_03_04_2023_08_46_v1.csv"
)
OUTPUT_FILE_PATH = "/output/Output - " + generate_output_filename()
