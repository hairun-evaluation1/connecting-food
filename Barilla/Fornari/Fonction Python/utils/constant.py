from utils.utils import generate_output_filename

NULL = None
CATEGORY = "raccolto"
LEVEL = "1"
FORNARI_UMBERTO = "FORNARI UMBERTO"
KG = "kg"
INPUT_FILE_PATH = "/data/20221027_Fornari_Retraité.csv"
OUTPUT_FILE_PATH = "/output/Output - " + generate_output_filename()
