import pandas as pd
import datetime
from utils.constant import INPUT_FILE_PATH

def transform_csv(source_file_path, output_path):

    now = datetime.datetime.now()
    df = pd.read_csv(source_file_path)

    df.columns = df.columns.str.lower()
    df.columns = df.columns.str.replace(' ', '_')

    dtt = now.strftime("%d_%m_%Y %H_%M_%S")
    df.to_csv(f'{output_path}output - {dtt}.csv', index=False)

    print("Finished")


if __name__ == "__main__":
    
    transform_csv(
        source_file_path=INPUT_FILE_PATH,

        # Dossier de sortie
        output_path="output/"
    )