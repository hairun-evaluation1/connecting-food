import os
import pandas as pd
from model.modelclass import Model_class
from utils.constant import INPUT_FILE_PATH, OUTPUT_FILE_PATH, NULL, STEP, PRODUCT , CATEGORY

SOURCE_PATH = os.path.dirname(os.path.abspath(__file__))


def format_csv(source_file_path, output_file_path):
    """Function used to transform an input csv into a formatted one"""

    df = pd.read_csv(source_file_path, delimiter=";", decimal=".", encoding="utf-8")
    df = df.dropna(how="all")
    column_number = df.shape[1]

    row_list = []

    # Check if column number of the input data exceed 10
    if column_number >= 10:
        print("Formatting...")

        # Get input data line by line

        for index, row in df.iterrows():
            quantite = str(row.get("Quantit\u00e9 de farine")).rstrip("0").rstrip(".")

            # Dict model
            factory_dict = {
                "category": CATEGORY,
                "step": STEP,
                "level": NULL,
                "capture_start": row.get("Date collecte"),
                "capture_end": NULL,
                "capture_uid": NULL,
                "sender_actor_number": row.get("Producteur"),
                "sender_location_number": NULL,
                "sender_asset_number": NULL,
                "sender_track_id": NULL,
                "sender_sku": NULL,
                "sender_batch_number": NULL,
                "sender_model_number": NULL,
                "sender_production_date": row.get("Date collecte"),
                "sender_product": PRODUCT,
                "sender_quantity": quantite,
                "sender_unit": NULL,
                "receiver_actor_number": row.get("Producteur"),
                "receiver_location_number": NULL,
                "receiver_asset_number": NULL,
                "receiver_track_id": row.get("Num\u00e9ro de lot"),
                "receiver_sku": NULL,
                "receiver_batch_number": NULL,
                "receiver_model_number": NULL,
                "receiver_production_date": NULL,
                "receiver_product": NULL,
                "receiver_quantity": NULL,
                "receiver_unit": NULL,
                "tags": NULL,
                "payload__date_collecte": row.get("Date collecte"),
                "payload__date_mouture": row.get("Date mouture"),
                "payload__lieu_mouture": row.get("Lieu mouture"),
                "payload__date_d_ensachage": row.get("Date d'ensachage"),
                "payload__dlc": row.get("DLC"),
            }

            row_list.append(Model_class(**factory_dict).__dict__)

        # Dataframe transformation
        result = pd.DataFrame(row_list)

        # Dataframe into CSV
        result.to_csv(output_file_path, index=False, sep=",", encoding="utf-8")
        print("Finished")
    else:
        print("There is not enough column")


if __name__ == "__main__":
    format_csv(
        source_file_path=SOURCE_PATH + INPUT_FILE_PATH,
        output_file_path=SOURCE_PATH + OUTPUT_FILE_PATH,
    )
