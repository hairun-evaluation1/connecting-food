from utils.utils import generate_output_filename

NULL = None
CATEGORY = "Expédition blé"
STEP = "shipping"

INPUT_FILE_PATH = "/data/BLKNAIA_0915220023_75000191.CSV"
OUTPUT_FILE_PATH = "/output/Output - " + generate_output_filename()
