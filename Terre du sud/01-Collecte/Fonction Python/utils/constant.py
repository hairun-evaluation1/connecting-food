from utils.utils import generate_output_filename

NULL = None
CATEGORY = "Collecte"
STEP = "receiving"
INPUT_FILE_PATH = (
    "/data/connectingfood_tds_sansogm_collecte_25_08_2020_23 _47_42_v1.csv"
)
OUTPUT_FILE_PATH = "/output/Output - " + generate_output_filename()
