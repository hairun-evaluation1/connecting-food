import os
import pandas as pd
from model.client_delivery import ClientDelivery
from utils.constant import (
    INPUT_FILE_PATH,
    OUTPUT_FILE_PATH,
    NULL,
    STEP,
    ACTOR_SENDER,
    ACTOR_RECEIVER,
    CATEGORY,
)

SOURCE_PATH = os.path.dirname(os.path.abspath(__file__))


def format_csv(source_file_path, output_file_path):
    """Function used to transform an input csv into a formatted one"""

    df = pd.read_excel(source_file_path)
    column_number = df.shape[1]
    row_list = []
    # Check if column number of the input data exceed 30
    if column_number >= 30:
        print("Formatting...")

        # Get input data line by line
        for index, row in df.iterrows():

            # Dict model
            quantity = str(row.get(27))

            client_delivery_dict = {
                "category": CATEGORY,
                "step": STEP,
                "level": NULL,
                "capture_start": row.get(19),
                "capture_end": NULL,
                "capture_uid": NULL,
                "sender_actor_number": ACTOR_SENDER,
                "sender_location_number": row.get(7),
                "sender_asset_number": NULL,
                "sender_track_id": row.get(5),
                "sender_sku": NULL,
                "sender_batch_number": NULL,
                "sender_model_number": NULL,
                "sender_production_date": row.get(19),
                "sender_product": row.get(0),
                "sender_quantity": quantity.replace(",", "."),
                "sender_unit": NULL,
                "receiver_actor_number": ACTOR_RECEIVER,
                "receiver_location_number": NULL,
                "receiver_asset_number": NULL,
                "receiver_track_id": row.get(5),
                "receiver_sku": NULL,
                "receiver_batch_number": NULL,
                "receiver_model_number": NULL,
                "receiver_production_date": NULL,
                "receiver_product": NULL,
                "receiver_quantity": NULL,
                "receiver_unit": NULL,
                "tags": NULL,
                "payload__commande_code": row.get(23),
                "payload__date_commande": row.get(22),
                "payload__bon_de_livraison": row.get(21),
                "payload__SSCC_Palette_Exped": row.get(2),
                "payload__DLC": row.get(6),
                "payload__CP": row.get(17),
                "payload__ville": row.get(18),
                "payload__facture_code": row.get(25),
                "payload__Qte.U.Fact": row.get(55),
                "payload__nb_colis": row.get(28),
                "payload__lot_externe": row.get(5),
                "payload__client": row.get(16),
            }
            row_list.append(ClientDelivery(**client_delivery_dict).__dict__)

        # Dataframe transformation
        result = pd.DataFrame(row_list)
        # Dataframe into CSV
        result.to_csv(output_file_path, index=False, sep=",")
        print("Finished")
    else:
        print("There is not enough column")


if __name__ == "__main__":

    format_csv(
        source_file_path=SOURCE_PATH + INPUT_FILE_PATH,
        output_file_path=SOURCE_PATH + OUTPUT_FILE_PATH,
    )
