import datetime


def generate_output_filename():
    now = datetime.datetime.now()
    return now.strftime("%d.%m.%Y.") + "4-Daco.Shipping_Vuw6ugB.csv"
