from utils.utils import generate_output_filename

NULL = None
CATEGORY = "Elevage"
STEP = "Commissioning"
INPUT_FILE_PATH = "/data/connectingfood_tds_sansogm_elevage_01_04_2023_23 _47_43_v1.csv"
OUTPUT_FILE_PATH = "/output/Output - " + generate_output_filename()
