from utils.utils import generate_output_filename

NULL = None
CATEGORY = "Mouture"
STEP = "processing"

INPUT_FILE_PATH = "/data/20220213_250_FrancineBIO_KOe6auo.csv"
OUTPUT_FILE_PATH = "/output/Output - " + generate_output_filename()
