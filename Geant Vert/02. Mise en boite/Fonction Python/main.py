import os
import pandas as pd
from model.boxing import Boxing
from utils.constant import (
    INPUT_FILE_PATH,
    OUTPUT_FILE_PATH,
    NULL,
    STEP,
    ACTOR,
    PRODUCT,
    QUANTITY,
    CATEGORY,
)

SOURCE_PATH = os.path.dirname(os.path.abspath(__file__))


def format_csv(source_file_path, output_file_path):
    """Function used to transform an input csv into a formatted one"""
    df = pd.read_csv(source_file_path, delimiter=";")
    column_number = df.shape[1]
    row_list = []

    # Check if column number of the input data exceed 4
    if column_number >= 4:
        print("Formatting...")

        # Get input data line by line
        for index, row in df.iterrows():

            quantieme = str(row.get("Codeof"))[:3]
            q = list(quantieme)
            if q[2] == "0":
                q[2] = "1"
            q.insert(2, "/0")
            quantieme = "".join(q)
            annee = str(row.get("Annee"))
            dluo = str(int(annee) + 4).strip()
            date = f"{quantieme}/{annee}"

            # Dict model
            boxing_dict = {
                "category": CATEGORY,
                "step": STEP,
                "level": NULL,
                "capture_start": date,
                "capture_end": NULL,
                "capture_uid": NULL,
                "sender_actor_number": ACTOR,
                "sender_location_number": NULL,
                "sender_asset_number": NULL,
                "sender_track_id": NULL,
                "sender_sku": NULL,
                "sender_batch_number": NULL,
                "sender_model_number": NULL,
                "sender_production_date": date,
                "sender_product": PRODUCT,
                "sender_quantity": QUANTITY,
                "sender_unit": NULL,
                "receiver_actor_number": ACTOR,
                "receiver_location_number": NULL,
                "receiver_asset_number": NULL,
                "receiver_track_id": row.get("Codeof"),
                "receiver_sku": NULL,
                "receiver_batch_number": NULL,
                "receiver_model_number": NULL,
                "receiver_production_date": NULL,
                "receiver_product": NULL,
                "receiver_quantity": NULL,
                "receiver_unit": NULL,
                "tags": NULL,
                "payload__salle": row.get("SalleReelle"),
                "payload__année_de_production": row.get("Annee"),
                "payload__DLUO": dluo,
                "payload__quantième_de_production": date,
            }

            row_list.append(Boxing(**boxing_dict).__dict__)

        # Dataframe transformation
        result = pd.DataFrame(row_list)

        # Dataframe into CSV
        result.to_csv(output_file_path, index=False, sep=",", encoding="utf-8")
        print("Finished")
    else:
        print("There is not enough column")


if __name__ == "__main__":

    format_csv(
        source_file_path=SOURCE_PATH + INPUT_FILE_PATH,
        output_file_path=SOURCE_PATH + OUTPUT_FILE_PATH,
    )
