import datetime


def generate_output_filename():
    now = datetime.datetime.now()
    return (
        "20201203_-_JUSTE_fichier_flux_dinfos_miel_sTmiNkK - "
        + now.strftime("%d_%m_%Y %H_%M_%S")
        + ".csv"
    )
