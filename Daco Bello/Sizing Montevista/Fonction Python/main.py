from datetime import timedelta
import os
import pandas as pd
from model.sizing import Sizing
from utils.constant import (
    INPUT_FILE_PATH,
    OUTPUT_FILE_PATH,
    NULL,
    STEP,
    CATEGORY,
    SENDER_ACTOR,
    RECEIVER_ACTOR,
    SITE,
)

SOURCE_PATH = os.path.dirname(os.path.abspath(__file__))


def format_csv(source_file_path, output_file_path):
    """Function used to transform an input csv into a formatted one"""
    df = pd.read_csv(source_file_path, delimiter=";", encoding="iso-8859-1")
    column_number = df.shape[1]
    row_list = []

    # Check if column number of the input data exceed 8
    if column_number >= 8:
        print("Formatting...")

        # Get input data line by line
        for index, row in df.iterrows():
            hour = pd.to_datetime(
                row.get("DateCreated"), format="%m/%d/%y %H:%M"
            ).strftime("%H:%M")
            sizer_won_number = row.get("SizerWONumber")
            sizer_batch_hour = f"{sizer_won_number}-{hour}"
            receiving_year = f"20{row.get('ReceivingYear')}"
            DLUO = (
                pd.to_datetime(row.get("DateCreated"), format="%m/%d/%y %H:%M")
                + timedelta(days=365)
            ).strftime("%d/%m/%y")
            batch_number = f"{row.get('FieldDesc')}-{receiving_year}-{row.get('VarietyDesc')}"

            # Dict model
            sizing_dict = {
                "category": CATEGORY,
                "step": STEP,
                "level": NULL,
                "capture_start": row.get("DateCreated"),
                "capture_end": NULL,
                "capture_uid": NULL,
                "sender_actor_number": SENDER_ACTOR,
                "sender_location_number": row.get("FieldDesc"),
                "sender_asset_number": NULL,
                "sender_track_id": batch_number,
                "sender_sku": NULL,
                "sender_batch_number": NULL,
                "sender_model_number": NULL,
                "sender_production_date": row.get("DateCreated"),
                "sender_product": row.get("VarietyDesc"),
                "sender_quantity": row.get("NetWeight"),
                "sender_unit": NULL,
                "receiver_actor_number": RECEIVER_ACTOR,
                "receiver_location_number": SITE,
                "receiver_asset_number": NULL,
                "receiver_track_id": row.get("SizerWONumber"),
                "receiver_sku": NULL,
                "receiver_batch_number": NULL,
                "receiver_model_number": NULL,
                "receiver_production_date": NULL,
                "receiver_product": NULL,
                "receiver_quantity": NULL,
                "receiver_unit": NULL,
                "tags": NULL,
                "payload__lot_de_tri": row.get("SizerWONumber"),
                "payload__batch_number": batch_number,
                "payload__block": row.get("FieldDesc"),
                "payload__receiving_year": receiving_year,
                "payload__DLUO": DLUO,
                "payload__varietyDesc": row.get("VarietyDesc"),
                "payload__hour": hour,
                "payload__sizer_Batch-Hour": sizer_batch_hour,
            }

            row_list.append(Sizing(**sizing_dict).__dict__)

        # Dataframe transformation
        result = pd.DataFrame(row_list)
        # Dataframe into CSV
        result.to_csv(output_file_path, index=False, sep=",")
        print("Finished")
    else:
        print("There is not enough column")


if __name__ == "__main__":

    format_csv(
        source_file_path=SOURCE_PATH + INPUT_FILE_PATH,
        output_file_path=SOURCE_PATH + OUTPUT_FILE_PATH,
    )
