import datetime


def generate_output_filename():
    now = datetime.datetime.now()
    return (
        "Lots (version4)_ZIZWBlr - "
        + now.strftime("%d_%m_%Y %H_%M_%S")
        + ".csv"
    )
