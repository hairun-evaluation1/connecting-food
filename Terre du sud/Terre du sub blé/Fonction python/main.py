import pandas as pd
import datetime


def transform_csv(source_file_path, output_path):

    now = datetime.datetime.now()
    df = pd.read_csv(source_file_path, delimiter=";", encoding='latin-1')
    index_of_sender_batch_number = df.columns.get_loc("sender_batch_number")

    df.insert(index_of_sender_batch_number + 1, "sender_batch_number_concat",df['sender_asset_number'].astype(str) + df['sender_batch_number'].astype(str))
    df['sender_batch_number_concat'] = df['sender_batch_number_concat'].str.replace(',', '.')
    dtt = now.strftime("%d_%m_%Y %H_%M_%S")
    
    df.to_csv(f'{output_path}output - {dtt}.csv', index=False)
    print("Finished")

if __name__ == "__main__":
    
    transform_csv(
        # A changer selon le nom du fichier à transformer
        source_file_path='data/Tracabilite[54].csv',

        # Dossier de sortie
        output_path="output/"
    )
