import datetime


def generate_output_filename():
    now = datetime.datetime.now()
    return (
        "PAE4-DataV0.3-Veto_xjYofVi - "
        + now.strftime("%d_%m_%Y %H_%M_%S")
        + ".csv"
    )
