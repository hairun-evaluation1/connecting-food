import pandas as pd

# Charger les deux fichiers CSV dans des DataFrames
df_reference = pd.read_csv('actor_ref.csv')
df_search = pd.read_csv('company_name.csv')


# Parcourir le DataFrame de référence
# for index, row in df_reference.iterrows():
#     actor_name_to_search = row['actor_number']
#     filtered_df = df_search[df_search['actor_number'] == actor_name_to_search]
    
#     # Vérifier s'il y a des résultats et les ajouter au dictionnaire
#     if not filtered_df.empty:
#         results[actor_name_to_search] = filtered_df

def get_actor(row):
        acteur_value = str(row["company_name"])
        matching_rows = df_reference[df_reference["name"] == acteur_value]

        if not matching_rows.empty:
            return str(matching_rows.iloc[0]["actor_number"])
        return None

df_search["result"] = df_search.apply(get_actor, axis=1)

# Sauvegarder le DataFrame de résultats dans un fichier CSV
df_search.to_csv('resultats.csv', index=False)

print("Les résultats ont été sauvegardés dans 'resultats.csv'.")
