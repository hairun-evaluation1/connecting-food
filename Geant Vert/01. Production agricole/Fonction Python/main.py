import os, csv
from datetime import datetime
import pandas as pd
from model.modelclass import Model_class
from utils.constant import (
    INPUT_FILE_PATH,
    OUTPUT_FILE_PATH,
    NULL,
    STEP,
    PRODUCT,
    QUANTITY,
    SITE_RECEIVER,
    ACTOR_RECEIVER,
    CATEGORY
)

SOURCE_PATH = os.path.dirname(os.path.abspath(__file__))


def format_csv(source_file_path, output_file_path):
    """Function used to transform an input csv into a formatted one"""

    df = pd.read_csv(source_file_path, delimiter=";", decimal=".", encoding="utf-8")

    column_number = df.shape[1]

    row_list = []

    # Check if column number of the input data exceed 9
    if column_number >= 9:
        print("Formatting...")

        # Get input data line by line
        for index, row in df.iterrows():
            date_fab = row.get("DateFab")
            datetime_object = datetime.strptime(date_fab, "%d/%m/%Y")
            day_string = str(datetime_object.day).zfill(2)
            transformed_date = datetime_object.strftime(f"{day_string}%Y")
            # Dict model
            factory_dict = {
                "category": CATEGORY,
                "step": STEP,
                "level": NULL,
                "capture_start": date_fab,
                "capture_end": NULL,
                "capture_uid": NULL,
                "sender_actor_number": str(row.get("ID_Agriculteur")).replace("-",""),
                "sender_location_number": row.get("ID_Parcelle"),
                "sender_asset_number": NULL,
                "sender_track_id": NULL,
                "sender_sku": NULL,
                "sender_batch_number": NULL,
                "sender_model_number": NULL,
                "sender_production_date": transformed_date,
                "sender_product": PRODUCT,
                "sender_quantity": QUANTITY,
                "sender_unit": NULL,
                "receiver_actor_number": ACTOR_RECEIVER,
                "receiver_location_number": SITE_RECEIVER,
                "receiver_asset_number": NULL,
                "receiver_track_id": transformed_date,
                "receiver_sku": NULL,
                "receiver_batch_number": NULL,
                "receiver_model_number": NULL,
                "receiver_production_date": NULL,
                "receiver_product": NULL,
                "receiver_quantity": NULL,
                "receiver_unit": NULL,
                "tags": NULL,
                "payload__heure_debut_prod": row.get("HDT"),
                "payload__heure_fin_prod": row.get("HFT"),
                "payload__salle": row.get("loST"),
                "payload__date_semis": row.get("DateSemis"),
                "payload__date_récolte": row.get("DateRecolte"),
            }

            row_list.append(Model_class(**factory_dict).__dict__)

        # Dataframe transformation
        result = pd.DataFrame(row_list)

        result["capture_start"] = result["capture_start"].astype(str)
        result["sender_production_date"] = result["sender_production_date"].astype(str)
        result["receiver_track_id"] = result["receiver_track_id"].astype(str)

        # Dataframe into CSV
        result.to_csv(output_file_path, index=False, sep=",", encoding="utf-8")
        print("Finished")
    else:
        print("There is not enough column")


if __name__ == "__main__":
    format_csv(
        source_file_path=SOURCE_PATH + INPUT_FILE_PATH,
        output_file_path=SOURCE_PATH + OUTPUT_FILE_PATH,
    )
