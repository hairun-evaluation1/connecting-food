import datetime


def generate_output_filename():
    now = datetime.datetime.now()
    return "20220304 Mouvement - " + now.strftime("%d_%m_%Y %H_%M_%S") + ".csv"
