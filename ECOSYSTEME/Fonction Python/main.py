import os, math
import pandas as pd
from model.modelclass import Model_class
from utils.constant import INPUT_FILE_PATH, NULL , PRODUCT , ASSET , LOCATION, ACTOR

SOURCE_PATH = os.path.dirname(os.path.abspath(__file__))


def format_csv(source_file_path, client, type_ecosysteme):
    """Function used to transform an input csv into a formatted one"""

    df = pd.read_csv(source_file_path, delimiter=",", encoding="utf-8" )
    
    row_list = []
    
    for index, row in df.iterrows():
        
        if type_ecosysteme == "actor":
            
            actor_number = str(row.get("Code Interne REL"))
        
            if actor_number == "nan":
                actor_number = row.get("ID acteur")

            actor_number = actor_number if str(actor_number).isdigit() else row.get("ID acteur")
            # actor_number = str(actor_number).replace("44867070300017-","").rstrip("0").rstrip(".")
            if "tmp_" in actor_number :
                continue
            
            acteur_name = str(row.get("Acteur")).rstrip("0").rstrip(".").replace("-","")

            # acteur_name = int(acteur_name)
            factory_dict = {
                "actor_number": actor_number,
                "name": acteur_name,
                "identification_type": "siret",
                "identification_number": row.get("ID acteur"),
                "company_name": NULL,
                "category": "undefined",
                "description": NULL,
                "tags": NULL,
                "sectors": NULL,
            }
        # à utiliser pour certains cas de client seulement

        # if type_ecosysteme == "actor":
        #     actor_number = str(row.get("Code Interne REL")).rstrip("0").rstrip(".")

        #     if actor_number == "nan":
        #         actor_number = row.get("ID acteur")

        #     factory_dict = {
        #         "actor_number": row.get("ID acteur"),
        #         "name": row.get("Nom de l'acteur"),
        #         "identification_type": "siret",
        #         "identification_number": row.get("ID acteur"),
        #         "company_name": NULL,
        #         "category": "undefined",
        #         "description": NULL,
        #         "tags": NULL,
        #         "sectors": NULL,
        #     }

        elif type_ecosysteme == "location":

            if type(row.get("Code interne")) == float:
                code = str(row.get("Code interne"))
                code_interne = str(code).rjust(4, '0') if len(code) == 2 or len(code) == 4 else str(code)
            else:
                code_interne = str(row.get("Code interne"))  
            code_interne = code_interne if code_interne != "nan" else NULL
            code_interne = str(code_interne).rstrip("0").rstrip(".").replace("-","")
            pays = str(row.get("Pays")) 
            pays = pays if pays != "nan" else "France"
            code_postal = str(row.get("Code postal")).rstrip("0").rstrip(".")
            line_1 = row.get("Adresse du site") if type(row.get("Adresse du site")) != float else "undefined"
            
            code_interne_rel = str(row.get("Code Interne REL")) if str(row.get("Code Interne REL")) != "nan" else NULL
            code_interne_rel = "undefined" if code_interne_rel is None else code_interne_rel
            code_interne_rel = code_interne if code_interne_rel == "undefined" else code_interne_rel
            code_interne_rel = code_interne_rel.replace("-","")
            code_interne_rel = str(code_interne_rel).rstrip("0").rstrip(".")
            factory_dict = {
                "location_number": code_interne ,
                "actor_number": code_interne_rel if code_interne != "" else code_interne,
                "name": row.get("Nom du site").replace("-",""),
                "company_name": row.get("Propriétaire").replace("-",""),
                "gln": NULL,
                "country": pays,
                "postcode": code_postal if code_postal != "nan" else NULL,
                "city": row.get("Ville"),
                "first_name": NULL,
                "last_name": NULL,
                "line_1": line_1,
                "line_2": NULL,
                "line_3": NULL,
                "tags": NULL,
                "latitude": NULL,
                "longitude": NULL,
            }

        elif type_ecosysteme == "asset":
            capacity = str(row.get("Capacité")).rstrip("0").rstrip(".")
            code = str(row.get("Code interne"))
            code_interne = str(code).rjust(2, '0') if len(code) == 2 else str(code)
            factory_dict = {
                "asset_number": code_interne,
                "name": row.get("Nom de l'outil"),
                "actor_number": row.get("Propriétaire"),
                "location_number": NULL,
                "category": NULL,
                "capacity": capacity if capacity != "nan" else NULL,
                "unit": row.get("Unité"),
                "tags": NULL,
            }
        elif type_ecosysteme == "product":

            factory_dict = {
                "product_number": row.get("Code interne"),
                "name": row.get("Nom du produit"),
                "description": NULL,
                "ean13": NULL,
                "gtin13": NULL,
                "gtin14": NULL,
                "height": NULL,
                "height_unit": NULL,
                "width_unit": NULL,
                "depth": NULL,
                "depth_unit": NULL,
                "volume": NULL,
                "volume_unit": NULL,
                "tags": NULL,
                "sectors": NULL,
            }

        row_list.append(Model_class(**factory_dict).__dict__)

    # Dataframe transformation
    result = pd.DataFrame(row_list)
    # Dataframe into CSV
    result.to_csv(f"output/{client}_{type_ecosysteme}.csv", index=False, sep=",", encoding="utf-8")

    file_name = f"{client}_{type_ecosysteme}"

    # Chargement du fichier CSV dans un DataFrame
    df = pd.read_csv(f'output/{file_name}.csv')

    # Suppression des lignes en doublon basées sur l'ensemble des colonnes

    if type_ecosysteme == "actor":
        df = df.drop_duplicates(subset="actor_number", keep="first")

    # Sauvegarde du DataFrame modifié dans un nouveau fichier CSV
    df.to_csv(f'output/{file_name}.csv', index=False)

    print("⭕⭕⭕⭕⭕⭕⭕")
    print(source_file_path)
    print("🟡🟡🟡🟡🟡🟡🟡")
    print(type_ecosysteme)
    print("🟢🟢🟢🟢🟢🟢🟢")
    print("Finished")


if __name__ == "__main__":
    format_csv(
        source_file_path=SOURCE_PATH + INPUT_FILE_PATH,
        type_ecosysteme=ACTOR,
        client="ingredia_fix"
    )
