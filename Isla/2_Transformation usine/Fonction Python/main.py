import os
import pandas as pd
from model.modelclass import Model_class
from utils.constant import (
    INPUT_FILE_PATH,
    OUTPUT_FILE_PATH,
    STEP,
    LEVEL,
    RECEIVER_ACTOR_NUMBER,
    NULL,
    SENDER_ACTOR_NUMBER,
    CATEGORY,
)

SOURCE_PATH = os.path.dirname(os.path.abspath(__file__))


def format_csv(source_file_path, output_file_path):
    """Function used to transform an input csv into a formatted one"""

    df = pd.read_csv(source_file_path, delimiter=";", header=None)
    column_number = df.shape[1]
    row_list = []

    # Check if column number of the input data exceed 15
    if column_number >= 15:
        print("Formatting...")

        # Get input data line by line
        for index, row in df.iterrows():
            receiver_track_id = str(row.get(9)).strip()
            sender_track_ids = row.get(11).split(",")

            # We create a specific line for every sender_track_id

            for sender_track_id in sender_track_ids:
                # Do not take in account line , when receiver_track_id = sender_track_id

                if receiver_track_id != str(sender_track_id).strip():
                    # Dict model

                    factory_dict = {
                        "category": CATEGORY,
                        "step": STEP,
                        "level": LEVEL,
                        "capture_start": row.get(7),
                        "capture_end": NULL,
                        "capture_uid": NULL,
                        "sender_actor_number": SENDER_ACTOR_NUMBER,
                        "sender_location_number": row.get(2),
                        "sender_asset_number": row.get(4),
                        "sender_track_id": sender_track_id.strip(),
                        "sender_sku": NULL,
                        "sender_batch_number": NULL,
                        "sender_model_number": NULL,
                        "sender_production_date": row.get(7),
                        "sender_product": NULL,
                        "sender_quantity": NULL,
                        "sender_unit": NULL,
                        "receiver_actor_number": RECEIVER_ACTOR_NUMBER,
                        "receiver_location_number": row.get(2),
                        "receiver_asset_number": row.get(4),
                        "receiver_track_id": row.get(9),
                        "receiver_sku": NULL,
                        "receiver_batch_number": NULL,
                        "receiver_model_number": NULL,
                        "receiver_production_date": NULL,
                        "receiver_product": str(row.get(5)).strip(),
                        "receiver_quantity": NULL,
                        "receiver_unit": NULL,
                        "tags": NULL,
                    }
                    row_list.append(Model_class(**factory_dict).__dict__)

        # Dataframe transformation
        result = pd.DataFrame(row_list)

        # Dataframe into CSV
        result.to_csv(output_file_path, index=False, sep=",")
        print("Finished")
    else:
        print("There is not enough column")


if __name__ == "__main__":
    format_csv(
        source_file_path=SOURCE_PATH + INPUT_FILE_PATH,
        output_file_path=SOURCE_PATH + OUTPUT_FILE_PATH,
    )
