import datetime


def generate_output_filename():
    now = datetime.datetime.now()
    return "Extraction_Factures_Herta - " + now.strftime("%d_%m_%Y %H_%M_%S") + ".csv"
