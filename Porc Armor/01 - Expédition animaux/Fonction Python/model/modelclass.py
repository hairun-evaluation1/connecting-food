class Model_class:

    """Data Model"""

    def __init__(self, **kwargs):
        for key, value in kwargs.items():
            setattr(self, key, value)
