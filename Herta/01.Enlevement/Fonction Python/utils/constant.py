from utils.utils import generate_output_filename

NULL = None
CATEGORY = "Enlevement"
STEP = "consigning"

INPUT_FILE_PATH = "/data/Extraction_Factures_Herta.csv"
OUTPUT_FILE_PATH = "/output/Output - " + generate_output_filename()
