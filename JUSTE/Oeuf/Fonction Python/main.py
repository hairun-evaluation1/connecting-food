import os
import pandas as pd
from model.egg import Egg
from utils.constant import (
    INPUT_FILE_PATH,
    OUTPUT_FILE_PATH,
    NULL,
    STEP,
    ACTOR,
    PRODUCT,
    CATEGORY,
)

SOURCE_PATH = os.path.dirname(os.path.abspath(__file__))


def format_csv(source_file_path, output_file_path):
    """Function used to transform an input csv into a formatted one"""

    df = pd.read_csv(
        source_file_path,
        delimiter=";",
        encoding="cp1252",
    )
    column_number = df.shape[1]
    row_list = []

    # Check if column number of the input data exceed 6
    if column_number >= 6:
        print("Formatting...")

        # Get input data line by line
        for index, row in df.iterrows():

            numero_lot = str(row.get("Numéro de lot")).replace(" ", "")

            if numero_lot != "nan" and len(numero_lot) > 0:
                # Dict model
                egg_dict = {
                    "category": CATEGORY,
                    "step": STEP,
                    "level": NULL,
                    "capture_start": row.get("Date de mise en boite"),
                    "capture_end": NULL,
                    "capture_uid": NULL,
                    "sender_actor_number": row.get("Producteur"),
                    "sender_location_number": NULL,
                    "sender_asset_number": NULL,
                    "sender_track_id": NULL,
                    "sender_sku": NULL,
                    "sender_batch_number": NULL,
                    "sender_model_number": NULL,
                    "sender_production_date": row.get("Date de mise en boite"),
                    "sender_product": PRODUCT,
                    "sender_quantity": row.get("Quantité (nombre d'œuf)"),
                    "sender_unit": NULL,
                    "receiver_actor_number": ACTOR,
                    "receiver_location_number": NULL,
                    "receiver_asset_number": NULL,
                    "receiver_track_id": numero_lot,
                    "receiver_sku": NULL,
                    "receiver_batch_number": NULL,
                    "receiver_model_number": NULL,
                    "receiver_production_date": NULL,
                    "receiver_product": NULL,
                    "receiver_quantity": NULL,
                    "receiver_unit": NULL,
                    "tags": NULL,
                    "payload__date_de_ponte": row.get("Date de ponte"),
                }

                row_list.append(Egg(**egg_dict).__dict__)

        # Dataframe transformation
        result = pd.DataFrame(row_list)
        # Dataframe into CSV
        result.to_csv(output_file_path, index=False, sep=",")
        print("Finished")
    else:
        print("There is not enough column")


if __name__ == "__main__":

    format_csv(
        source_file_path=SOURCE_PATH + INPUT_FILE_PATH,
        output_file_path=SOURCE_PATH + OUTPUT_FILE_PATH,
    )
