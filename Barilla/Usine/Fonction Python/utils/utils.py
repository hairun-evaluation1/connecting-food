def generate_output_filename():
    return "130_Prd_Rintracciabilita_MP.csv"


def remove_brackets(str_input: str):
    return (
        str_input.replace("\\ufeff", "")
        .replace("['", "")
        .replace("']", "")
        .replace("'", "")
        .replace(" ", "")
    )
