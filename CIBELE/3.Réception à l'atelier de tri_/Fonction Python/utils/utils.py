import datetime


def generate_output_filename():
    now = datetime.datetime.now()
    return (
        "20220610 Reception - Copie - "
        + now.strftime("%d_%m_%Y %H_%M_%S")
        + ".csv"
    )
