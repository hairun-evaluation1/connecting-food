import os, sys, unittest, json
import pandas as pd

sys.path.append(os.path.abspath(".."))
from main import format_csv


def nan_to_None(s: str):
    return s if s != "nan" else "None"


SOURCE_PATH = os.path.dirname(os.path.abspath(__file__))
INPUT_PATH = SOURCE_PATH + "/input"
OUTPUT_PATH = SOURCE_PATH + "/output"
CONF_PATH = SOURCE_PATH + "/conf/conf.json"

with open(CONF_PATH) as f:
    conf = json.load(f)


class TestStringMethods(unittest.TestCase):
    def test_format_csv(self):
        # Define input and expected output paths
        input_file_path = INPUT_PATH + "/input.csv"
        output_file_path = OUTPUT_PATH + "/actual_output.csv"

        # Call the function to be tested
        format_csv(source_file_path=input_file_path, output_file_path=output_file_path)

        # Load the input and output files as dataframes
        input_df = pd.read_csv(
            input_file_path, delimiter=";", decimal=".", encoding="iso-8859-1"
        )

        output_df = pd.read_csv(output_file_path, delimiter=",")

        # Loop input and output line by line
        for (index1, row1), (index2, row2) in zip(
            input_df.iterrows(), output_df.iterrows()
        ):
            for key, value in conf.items():
                if key in [
                    "step",
                    "category",
                    "sender_actor_number",
                ]:
                    expr1 = str(value).strip()
                    expr2 = str(row2.get(key))
                elif key == "sender_quantity":
                    expr1 = (
                        str(row1.get(value))
                        .replace(",", ".")
                        .replace("-", "")
                        .rstrip("0")
                        .rstrip(".")
                    )
                    expr2 = str(row2.get(key)).rstrip("0").rstrip(".")
                elif key == "payload__cv":
                    chrono_str = str(row1.get(value))
                    expr1 = f"Chrono VIF:{chrono_str}"
                    expr2 = str(row2.get(key))
                else:
                    expr1 = str(row1.get(value))
                    expr2 = str(row2.get(key))

                try:
                    self.assertEqual(
                        nan_to_None(str(expr1).strip()),
                        nan_to_None(str(expr2).strip()),
                        msg=f"{expr1} != {expr2}",
                    )
                except AssertionError as e:
                    print(f"key:{key} >> erreur : {expr1} != {expr2}")


if __name__ == "__main__":
    unittest.main()
