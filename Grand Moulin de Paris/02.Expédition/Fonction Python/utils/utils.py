import datetime


def generate_output_filename():
    now = datetime.datetime.now()
    return now.strftime("%Y%m%d") + "_250_FrancineBIO_w4oQQWj.csv"
