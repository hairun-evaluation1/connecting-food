import os
import pandas as pd
from model.modelclass import Model_class
from utils.constant import (
    INPUT_FILE_PATH,
    OUTPUT_FILE_PATH,
    NULL,
    STEP,
    ACTOR_SENDER,
    CATEGORY,
)
from utils.utils import decimal_transformation

SOURCE_PATH = os.path.dirname(os.path.abspath(__file__))


def format_csv(source_file_path, output_file_path):
    """Function used to transform an input csv into a formatted one"""

    df = pd.read_csv(source_file_path, delimiter=";", decimal=".", encoding="utf-8")

    column_number = df.shape[1]

    row_list = []

    # Check if column number of the input data exceed 22
    if column_number >= 22:
        print("Formatting...")

        # Get input data line by line

        for index, row in df.iterrows():
            quantite_livre = decimal_transformation(str(row.get("quantite_livree")))
            mp = decimal_transformation(str(row.get("mp")))
            mg = decimal_transformation(str(row.get("mg")))
            cellulose = decimal_transformation(str(row.get("cellulose")))
            phosphore = decimal_transformation(str(row.get("phosphore")))

            presentation = str(row.get("presentation"))

            if presentation == "GR" or "GR" in presentation:
                presentation = "Granul\u00e9"

            elif presentation == "FAR.":
                presentation = "Farine"

            elif presentation == "SEM.":
                presentation = "Semoule"

            type_conditionnement = str(row.get("type_conditionnement"))

            if type_conditionnement == "V":
                type_conditionnement = "Vrac"

            elif type_conditionnement == "S":
                type_conditionnement = "Sac"

            # Dict model
            factory_dict = {
                "category": CATEGORY,
                "step": STEP,
                "level": NULL,
                "capture_start": row.get("date_livraison"),
                "capture_end": NULL,
                "capture_uid": NULL,
                "sender_actor_number": ACTOR_SENDER,
                "sender_location_number": row.get("fournisseur"),
                "sender_asset_number": NULL,
                "sender_track_id": NULL,
                "sender_sku": NULL,
                "sender_batch_number": NULL,
                "sender_model_number": NULL,
                "sender_production_date": row.get("date_livraison"),
                "sender_product": row.get("code_article"),
                "sender_quantity": quantite_livre,
                "sender_unit": NULL,
                "receiver_actor_number": row.get("idm"),
                "receiver_location_number": row.get("idm"),
                "receiver_asset_number": NULL,
                "receiver_track_id": row.get("idm"),
                "receiver_sku": NULL,
                "receiver_batch_number": NULL,
                "receiver_model_number": NULL,
                "receiver_production_date": NULL,
                "receiver_product": NULL,
                "receiver_quantity": NULL,
                "receiver_unit": NULL,
                "tags": NULL,
                "payload__bl": row.get("numero_bl"),
                "payload__type_d_aliments": row.get("aliment"),
                "payload__idm": row.get("idm"),
                "payload__mp": mp,
                "payload__mg": mg,
                "payload__cellulose": cellulose,
                "payload__phosphore": phosphore,
                "payload__présentation": presentation,
                "payload__type_de_conditionnement": type_conditionnement,
                "payload__destination": row.get("destination"),
            }

            row_list.append(Model_class(**factory_dict).__dict__)

        # Dataframe transformation
        result = pd.DataFrame(row_list)

        # Dataframe into CSV
        result.to_csv(output_file_path, index=False, sep=",", encoding="utf-8")
        print("Finished")
    else:
        print("There is not enough column")


if __name__ == "__main__":
    format_csv(
        source_file_path=SOURCE_PATH + INPUT_FILE_PATH,
        output_file_path=SOURCE_PATH + OUTPUT_FILE_PATH,
    )
