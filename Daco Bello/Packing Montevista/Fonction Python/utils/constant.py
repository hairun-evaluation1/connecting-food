from utils.utils import generate_output_filename

NULL = None
CATEGORY = "Packing Montevista"
STEP = "packing"
ACTOR_SENDER = "USMONTEVISTA"
SITE_SENDER = "PACKER"
INPUT_FILE_PATH = "/data/Daco_Blockchain_-_Packing_DataV6_298_fin.csv"
OUTPUT_FILE_PATH = "/output/Output - " + generate_output_filename()
