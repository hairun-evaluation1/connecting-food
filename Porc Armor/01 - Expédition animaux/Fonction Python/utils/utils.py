import datetime


def generate_output_filename():
    now = datetime.datetime.now()
    return "Extraction_BDPorc_" + now.strftime("%d_%m_%Y") + ".csv"
