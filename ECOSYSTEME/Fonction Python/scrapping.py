from bs4 import BeautifulSoup
import pandas as pd
from utils.constant import PRODUCT , ASSET , LOCATION, ACTOR

def extract_data(html_input , rel = False) -> tuple:
    soup = BeautifulSoup(html_input, "html.parser")
    table = soup.find("table")
    headers = [
        header.text.strip().replace("\n", "").replace("Oui", "").replace("Non", "")
        if header.text != "Ambassadeur\n\n\n\n\n\n\nTous\n\n\n\nTous\n\n\nOui\n\n\nNon"
        else "Ambassadeur"
        for header in table.select("thead tr th")
    ]

    headers = list(filter(None, headers))

    data_rows = table.select("tbody tr")

    data = [[cell.text.strip() for cell in row.select("td")] for row in data_rows]

    if rel is False:
        data = [row[1:] for row in data]
    else:
        data = [row[1:-1] for row in data]

    return data, headers


def relational_source_csv(client, type_ecosysteme):
    with open('main.html', 'r') as file:
        html_content = file.read()

    with open('relation.html', 'r') as file:
        html_content_relation = file.read()

    data, headers = extract_data(html_content)
    # Put REL == FALSE  si pas d' extra colonne dans RELATION , True au contraire
    data_rel, headers_rel = extract_data(html_content_relation, rel = True)

    df_rel = pd.DataFrame(data_rel, columns=headers_rel)

    
    df = pd.DataFrame(data, columns=headers)

    if type_ecosysteme == ACTOR:
        foreign_key =  "Nom de l'acteur"
    elif type_ecosysteme == LOCATION:
        foreign_key =  "Propriétaire"

    def get_code_interne(row):
        acteur_value = str(row[foreign_key])
        matching_rows = df_rel[df_rel["Acteur"] == acteur_value]

        if not matching_rows.empty:
            return matching_rows["Code Interne"].tolist()
        return None
    
    def get_actor(row):
        acteur_value = str(row["Nom de l'acteur"])
        matching_rows = df_rel[df_rel["Acteur"] == acteur_value]

        if not matching_rows.empty:
            return matching_rows.iloc[0]["Acteur"]
        return None

    df["Code Interne REL"] = df.apply(get_code_interne, axis=1)

    # Explode the list into multiple rows
    df = df.explode("Code Interne REL", ignore_index=True)

    if type_ecosysteme == ACTOR:
        df["Acteur"] = df.apply(get_actor, axis=1)


    df.to_csv(
        f"data/{client}_{type_ecosysteme}_source.csv",
        index=False,
        sep=",",
        encoding="utf-8",
    )
    print("Source obtained !")

def source_csv(client, type_ecosysteme):

    with open('main.html', 'r') as file:
        html_content = file.read()

    data, headers = extract_data(html_content)
    df = pd.DataFrame(data, columns=headers)


    df.to_csv(
        f"data/{client}_{type_ecosysteme}_source.csv",
        index=False,
        sep=",",
        encoding="utf-8",
    )
    print("Source obtained !")

if __name__ == "__main__":
    relational_source_csv(client="ingredia_fix", type_ecosysteme=ACTOR)
    # source_csv(client="porc_armor", type_ecosysteme=PRODUCT)
