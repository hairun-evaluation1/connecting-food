from utils.utils import generate_output_filename

NULL = None
CATEGORY = "Livraison usine"
STEP = "receiving"

INPUT_FILE_PATH = (
    "/data/connectingfood_tds_sansogm_livraison3_16_04_2020_23 _48_38_v1.csv"
)
OUTPUT_FILE_PATH = "/output/Output - " + generate_output_filename()
