import datetime


def generate_output_filename():
    now = datetime.datetime.now()
    return (
        "connectingfood_ingredia_vialacta_TRANSFORMATION_"
        + now.strftime("%d_%m_%Y %H_%M_%S")
        + ".csv"
    )
