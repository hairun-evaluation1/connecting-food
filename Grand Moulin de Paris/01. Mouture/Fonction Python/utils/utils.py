import datetime


def generate_output_filename():
    now = datetime.datetime.now()
    return (
        "20220213_250_FrancineBIO_KOe6auo - "
        + now.strftime("%d_%m_%Y %H_%M_%S")
        + ".csv"
    )
