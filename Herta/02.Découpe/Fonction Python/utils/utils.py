import datetime


def generate_output_filename():
    now = datetime.datetime.now()
    return "HERTA_OF_" + now.strftime("%d_%m_%Y %H_%M_%S") + ".csv"
