import os
import pandas as pd
from model.modelclass import Model_class
from utils.constant import (
    INPUT_FILE_PATH,
    OUTPUT_FILE_PATH,
    NULL,
    STEP,
    CATEGORY,
    ACTOR,
    SITE,
)

SOURCE_PATH = os.path.dirname(os.path.abspath(__file__))


def format_csv(source_file_path, output_file_path):
    """Function used to transform an input csv into a formatted one"""

    df = pd.read_csv(source_file_path, delimiter=";", decimal=".", encoding="utf-8")
    column_number = df.shape[1]

    row_list = []

    # Check if column number of the input data exceed 22
    if column_number >= 22:
        print("Formatting...")
        # Get input data line by line

        for index, row in df.iterrows():
            quantity = (
                str(row.get("Quantit\u00e9 ensach\u00e9e (kg)"))
                .strip()
                .replace(",", ".")
            )
            # Dict model
            factory_dict = {
                "category": CATEGORY,
                "step": STEP,
                "level": NULL,
                "capture_start": row.get("Date de production"),
                "capture_end": NULL,
                "capture_uid": NULL,
                "sender_actor_number": ACTOR,
                "sender_location_number": SITE,
                "sender_asset_number": row.get("Code interne outil de production"),
                "sender_track_id": row.get("N\u00b0 lot mouture"),
                "sender_sku": NULL,
                "sender_batch_number": NULL,
                "sender_model_number": NULL,
                "sender_production_date": row.get("Date de production"),
                "sender_product": row.get("Code produit sachets farine"),
                "sender_quantity": quantity,
                "sender_unit": NULL,
                "receiver_actor_number": ACTOR,
                "receiver_location_number": NULL,
                "receiver_asset_number": NULL,
                "receiver_track_id": row.get("DDM (Date de Durabilit\u00e9 Minimale)"),
                "receiver_sku": NULL,
                "receiver_batch_number": NULL,
                "receiver_model_number": NULL,
                "receiver_production_date": NULL,
                "receiver_product": NULL,
                "receiver_quantity": NULL,
                "receiver_unit": NULL,
                "tags": NULL,
                "payload__numero_lot_entrant": row.get("N\u00b0 lot mouture"),
                "payload__quantité_entrante": row.get("Quant. Sortante mouture (kg)"),
                "payload__code_produit_sortant": row.get("Code produit sachets farine"),
                "payload__code_interne_outil_de_production": row.get(
                    "Code interne outil de production"
                ),
                "payload__heure_démarrage": row.get("Heure d\u00e9marrage"),
                "payload__ddm": row.get("DDM (Date de Durabilit\u00e9 Minimale)"),
            }

            row_list.append(Model_class(**factory_dict).__dict__)

        # Dataframe transformation
        result = pd.DataFrame(row_list)

        # Dataframe into CSV
        result.to_csv(output_file_path, index=False, sep=",", encoding="utf-8")
        print("Finished")
    else:
        print("There is not enough column")


if __name__ == "__main__":
    format_csv(
        source_file_path=SOURCE_PATH + INPUT_FILE_PATH,
        output_file_path=SOURCE_PATH + OUTPUT_FILE_PATH,
    )
