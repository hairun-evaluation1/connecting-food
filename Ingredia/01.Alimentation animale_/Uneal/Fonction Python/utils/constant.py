from utils.utils import generate_output_filename

NULL = None
CATEGORY = "Alimentation animale Uneal"
STEP = "receiving"
INPUT_FILE_PATH = "/data/F_UNE_PFE_TRA_03_04_2023_08_00_00.csv"
OUTPUT_FILE_PATH = "/output/Output - " + generate_output_filename()
