import os
import pandas as pd
from model.packaging import Packaging
from utils.constant import (
    INPUT_FILE_PATH,
    OUTPUT_FILE_PATH,
    NULL,
    STEP,
    ACTOR,
    QUANTITY,
    SITE_RECEIVER,
    CATEGORY,
)

SOURCE_PATH = os.path.dirname(os.path.abspath(__file__))


def format_csv(source_file_path, output_file_path):
    """Function used to transform an input csv into a formatted one"""

    df = pd.read_csv(source_file_path, delimiter=";")
    column_number = df.shape[1]
    row_list = []

    # Check if column number of the input data exceed 9
    if column_number >= 9:
        print("Formatting...")

        # Get input data line by line
        for index, row in df.iterrows():

            # Dict model
            packaging_dict = {
                "category": CATEGORY,
                "step": STEP,
                "level": NULL,
                "capture_start": row.get("Date Récolte"),
                "capture_end": NULL,
                "capture_uid": NULL,
                "sender_actor_number": ACTOR,
                "sender_location_number": row.get("Lieu Parcelle"),
                "sender_asset_number": NULL,
                "sender_track_id": NULL,
                "sender_sku": NULL,
                "sender_batch_number": NULL,
                "sender_model_number": NULL,
                "sender_production_date": row.get("Date Récolte"),
                "sender_product": row.get("Type Produits"),
                "sender_quantity": QUANTITY,
                "sender_unit": NULL,
                "receiver_actor_number": ACTOR,
                "receiver_location_number": SITE_RECEIVER,
                "receiver_asset_number": NULL,
                "receiver_track_id": row.get("Numéro de lot").replace(" ", ""),
                "receiver_sku": NULL,
                "receiver_batch_number": NULL,
                "receiver_model_number": NULL,
                "receiver_production_date": NULL,
                "receiver_product": NULL,
                "receiver_quantity": NULL,
                "receiver_unit": NULL,
                "tags": NULL,
                "payload__date_semis_/_plantation": row.get(
                    "Date Semis / Plantation"
                ),
                "payload__date_récolte": row.get("Date Récolte"),
                "payload__certificat": row.get("Certificat "),
                "payload__marque": row.get("Marque"),
                "payload__référence": row.get("Référence"),
            }

            row_list.append(Packaging(**packaging_dict).__dict__)

        # Dataframe transformation
        result = pd.DataFrame(row_list)
        # Dataframe into CSV
        result.to_csv(output_file_path, index=False, sep=",")
        print("Finished")
    else:
        print("There is not enough column")


if __name__ == "__main__":

    format_csv(
        source_file_path=SOURCE_PATH + INPUT_FILE_PATH,
        output_file_path=SOURCE_PATH + OUTPUT_FILE_PATH,
    )
