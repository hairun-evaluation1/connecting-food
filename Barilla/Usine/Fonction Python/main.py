import os, csv
import pandas as pd
from model.modelclass import Model_class
from utils.constant import (
    INPUT_FILE_PATH,
    OUTPUT_FILE_PATH,
    NULL,
    LEVEL,
    BARILLA,
    KG,
    DATA_PRODUZIONE,
    GIORNO,
    CATEGORY
)
from utils.utils import remove_brackets

SOURCE_PATH = os.path.dirname(os.path.abspath(__file__))


def format_csv(source_file_path, output_file_path):
    """Function used to transform an input csv into a formatted one"""

    df = pd.read_csv(source_file_path, delimiter=",", skiprows=3, encoding="utf-8")

    column_number = df.shape[1]

    # Open the CSV file for reading the first two lines
    with open(source_file_path, newline="", encoding="utf-8") as csvfile:
        # Create a CSV reader object
        reader = csv.reader(csvfile, delimiter=",")

        # Read the first two rows of the file
        row1 = remove_brackets(str(next(reader)))
        row2 = str(next(reader))
        row2 = eval(row2)

    line1 = row1.split(",")
    data_produzione = (
        row2[line1.index(DATA_PRODUZIONE)] if DATA_PRODUZIONE in line1 else NULL
    )
    giorno_clc = row2[line1.index(GIORNO)] if GIORNO in line1 else NULL
    row_list = []

    # Check if column number of the input data exceed 11
    if column_number >= 11:
        print("Formatting...")

        # Get input data line by line

        for index, row in df.iterrows():
            # Dict model
            factory_dict = {
                "category": CATEGORY,
                "step": NULL,
                "level": LEVEL,
                "capture_start": data_produzione,
                "capture_end": NULL,
                "capture_uid": NULL,
                "sender_actor_number": row.get("Fornitore"),
                "sender_location_number": row.get("Fornitore"),
                "sender_asset_number": NULL,
                "sender_track_id": giorno_clc,
                "sender_sku": NULL,
                "sender_batch_number": row.get("N__Lotto_in_entrata__Fornitore_"),
                "sender_model_number": NULL,
                "sender_production_date": data_produzione,
                "sender_product": row.get("Prodotto_in_entrata__Articolo_"),
                "sender_quantity": row.get("Quantità_lotto_in_entrata__kg_"),
                "sender_unit": KG,
                "receiver_actor_number": BARILLA,
                "receiver_location_number": BARILLA,
                "receiver_asset_number": row.get("Linea_di_produzione"),
                "receiver_track_id": giorno_clc,
                "receiver_sku": NULL,
                "receiver_batch_number": row.get("N__Lotto_in_entrata__Fornitore_"),
                "receiver_model_number": NULL,
                "receiver_production_date": NULL,
                "receiver_product": row.get("Prodotto_finito"),
                "receiver_quantity": row.get("Quantità_dosata"),
                "receiver_unit": KG,
                "tags": row.get("Trasformazione"),
                "payload__ordine_de_produzione": row.get(
                    "Ordine_di_produzione_di_semilavorato"
                ),
                "payload__codice_partita": row.get("Codice_partita_Barilla"),
                "payload__data_di_ricezione": row.get("Data_di_ricezione"),
                "payload__turno": row.get("Turno"),
            }

            row_list.append(Model_class(**factory_dict).__dict__)

        # Dataframe transformation
        result = pd.DataFrame(row_list)

        # Dataframe into CSV
        result.to_csv(output_file_path, index=False, sep=",", encoding="utf-8")
        print("Finished")
    else:
        print("There is not enough column")


if __name__ == "__main__":
    format_csv(
        source_file_path=SOURCE_PATH + INPUT_FILE_PATH,
        output_file_path=SOURCE_PATH + OUTPUT_FILE_PATH,
    )
