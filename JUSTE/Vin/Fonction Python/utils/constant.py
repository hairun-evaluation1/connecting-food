from utils.utils import generate_output_filename

NULL = None
CATEGORY = "Vin"
STEP = "processing"
PRODUCT = "Vin Juste"

INPUT_FILE_PATH = (
    "/data/20220729 - Fichier traçabilité mise en bouteille du 23 05 22.csv"
)
OUTPUT_FILE_PATH = "/output/Output - " + generate_output_filename()
