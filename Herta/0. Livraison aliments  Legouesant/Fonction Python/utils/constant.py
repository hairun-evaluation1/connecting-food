from utils.utils import generate_output_filename

NULL = None
CATEGORY = "Livraison aliments Legouesant"
STEP = "receiving"

INPUT_FILE_PATH = "/data/Echange_20200930040656.csv"
OUTPUT_FILE_PATH = "/output/Output - " + generate_output_filename()
