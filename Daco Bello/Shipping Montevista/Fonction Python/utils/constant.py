from utils.utils import generate_output_filename

NULL = None
CATEGORY = "Shipping Montevista"
STEP = "consigning"
ACTOR_SENDER = "USMONTEVISTA"
SITE_SENDER = "PACKER"
PRODUCT = "FinishedGoods"
ACTOR_RECEIVER = "30048291600032"
INPUT_FILE_PATH = "/data/21.11.30.4-Daco.Shipping_Vuw6ugB.csv"
OUTPUT_FILE_PATH = "/output/Output - " + generate_output_filename()
