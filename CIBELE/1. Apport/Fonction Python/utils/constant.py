from utils.utils import generate_output_filename

NULL = None
CATEGORY = "Apport"
STEP = "receiving"

INPUT_FILE_PATH = "/data/1.Mouvements_mai_2019.csv"
OUTPUT_FILE_PATH = "/output/Output - " + generate_output_filename()
