from utils.utils import generate_output_filename

NULL = None
CATEGORY = "Découpe"
STEP = "processing"

INPUT_FILE_PATH = "/data/HERTA_OF_02774811_20230315_145045.csv"
OUTPUT_FILE_PATH = "/output/Output - " + generate_output_filename()
