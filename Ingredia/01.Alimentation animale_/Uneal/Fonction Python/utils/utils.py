import datetime


def generate_output_filename():
    now = datetime.datetime.now()
    return "F_UNE_PFE_TRA_" + now.strftime("%Y_%m_%d") + ".csv"
