from utils.utils import generate_output_filename

NULL = None
CATEGORY = "Expédition"
STEP = "consigning"
INPUT_FILE_PATH = "/data/bc_herta_2022_1012_p21_eng.csv"
OUTPUT_FILE_PATH = "/output/Output - " + generate_output_filename()
