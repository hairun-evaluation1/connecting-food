from utils.utils import generate_output_filename

NULL = None
CATEGORY = "Fruit concentrate"
ORIGIN_FRUIT = "Farmer delivery"
STEP = "processing"
ACTOR = "302005341"
SITE = "Mainfrucht"
INPUT_FILE_PATH = "/data/2_fruit_concentration.csv"
OUTPUT_FILE_PATH = "/output/Output - " + generate_output_filename()
