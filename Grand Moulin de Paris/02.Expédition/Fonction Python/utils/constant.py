from utils.utils import generate_output_filename

NULL = None
CATEGORY = "Expédition"
STEP = "shipping"
INPUT_FILE_PATH = "/data/20220814_250_FrancineBIO_w4oQQWj.csv"
OUTPUT_FILE_PATH = "/output/Output - " + generate_output_filename()
