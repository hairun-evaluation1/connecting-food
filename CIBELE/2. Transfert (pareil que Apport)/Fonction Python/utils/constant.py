from utils.utils import generate_output_filename

NULL = None
CATEGORY = "Transfert"
STEP = "stocking"
ACTOR = "39198157800035"

INPUT_FILE_PATH = "/data/20220304 Mouvement.csv"
OUTPUT_FILE_PATH = "/output/Output - " + generate_output_filename()
