from utils.utils import generate_output_filename

NULL = None
CATEGORY = "Lait"
STEP = "processing"
PRODUCT = "Lait Demi écrémé 1L JUSTE & VENDÉEN"

INPUT_FILE_PATH = "/data/20230123_-_JUSTE__VENDEEN_fichier_traca_collecte_du_20230116_modifie.csv"
OUTPUT_FILE_PATH = "/output/Output - " + generate_output_filename()
