from utils.utils import generate_output_filename

NULL = None
PRODUCT = "Lait Demi \u00e9cr\u00e9m\u00e9 1L JUSTE de LA"
CATEGORY = "Lait LA"
STEP = "processing"
INPUT_FILE_PATH = "/data/20230117 - Fichier traça JUSTE de LA Collecte du 20230109.csv"
OUTPUT_FILE_PATH = "/output/Output - " + generate_output_filename()
