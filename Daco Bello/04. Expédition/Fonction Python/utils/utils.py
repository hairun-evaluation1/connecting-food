import datetime


def generate_output_filename():
    now = datetime.datetime.now()
    return now.strftime("%Y%m%d") + "_dacobello_blockchain.csv"
