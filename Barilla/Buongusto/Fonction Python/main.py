import os
import pandas as pd
from model.model_class import ModelClass
from utils.constant import (
    INPUT_FILE_PATH,
    OUTPUT_FILE_PATH,
    NULL,
    CATEGORY,
    LEVEL,
    SENDER_ACTOR,
    RECEIVER_ACTOR,
    UNIT,
)

SOURCE_PATH = os.path.dirname(os.path.abspath(__file__))


def format_csv(source_file_path, output_file_path):
    """Function used to transform an input csv into a formatted one"""

    df = pd.read_csv(source_file_path, delimiter=",")
    column_number = df.shape[1]
    row_list = []

    # Check if column number of the input data exceed 14
    if column_number >= 14:
        print("Formatting...")

        # Get input data line by line
        for index, row in df.iterrows():

            # Dict model
            buongusto_dict = {
                "category": CATEGORY,
                "step": NULL,
                "level": LEVEL,
                "capture_start": row.get("Data di raccolta"),
                "capture_end": NULL,
                "capture_uid": NULL,
                "sender_actor_number": SENDER_ACTOR,
                "sender_location_number": row.get("Agricultore d'origine"),
                "sender_asset_number": row.get("Agricultore d'origine"),
                "sender_track_id": row.get("Lotto in uscita standard"),
                "sender_sku": NULL,
                "sender_batch_number": row.get("Lotto in uscita standard"),
                "sender_model_number": NULL,
                "sender_production_date": row.get("Data di raccolta"),
                "sender_product": row.get("Prodotto"),
                "sender_quantity": row.get("Peso (kg)"),
                "sender_unit": UNIT,
                "receiver_actor_number": RECEIVER_ACTOR,
                "receiver_location_number": row.get("Agricultore d'origine"),
                "receiver_asset_number": row.get("Agricultore d'origine"),
                "receiver_track_id": row.get("Lotto in uscita standard"),
                "receiver_sku": NULL,
                "receiver_batch_number": row.get("Lotto in uscita standard"),
                "receiver_model_number": NULL,
                "receiver_production_date": NULL,
                "receiver_product": NULL,
                "receiver_quantity": NULL,
                "receiver_unit": NULL,
                "tags": row.get("Lotto in uscita standard"),
                "payload__varieta": row.get("Varietà"),
                "payload__ numero_di_taglio": row.get("Numero di taglio"),
                "payload__numero_ddt": row.get("Numero di DDT"),
                "payload__certificato_iscc_plus": row.get(
                    "Numero certificato ISCC Plus"
                ),
                "payload__iscc_compliant": row.get("ISCC, ISCC PLUS"),
                "payload__criteri_sostenibilita": row.get(
                    "Criteri sostenibilità"
                ),
                "payload__tipo_di_custodia": row.get("Catena di custodia"),
            }

            row_list.append(ModelClass(**buongusto_dict).__dict__)

        # Dataframe transformation
        result = pd.DataFrame(row_list)
        # Dataframe into CSV
        result.to_csv(output_file_path, index=False, sep=",")
        print("Finished")
    else:
        print("There is not enough column")


if __name__ == "__main__":

    format_csv(
        source_file_path=SOURCE_PATH + INPUT_FILE_PATH,
        output_file_path=SOURCE_PATH + OUTPUT_FILE_PATH,
    )
