from utils.utils import generate_output_filename

NULL = None
CATEGORY = "Réception à l'atelier de tri"
STEP = "shipping"
ACTOR = "39198157800035"
SITE = "2930"

INPUT_FILE_PATH = "/data/20220610 Reception - Copie.csv"
OUTPUT_FILE_PATH = "/output/Output - " + generate_output_filename()
