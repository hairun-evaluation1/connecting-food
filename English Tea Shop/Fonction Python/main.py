import pandas as pd
import datetime


def transform_csv(source_file_path, output_path):

    now = datetime.datetime.now()
    df = pd.read_csv(source_file_path)

    df.columns = df.columns.str.lower()
    df.columns = df.columns.str.replace(' ', '_')

    index_of_sender_style_no = df.columns.get_loc("style_no")
    df.insert(index_of_sender_style_no + 1, "range_code",df['style_no'].apply(lambda x: 'err' if x.startswith('BR') else 'egr' if x.startswith('U') else None))
 

    dtt = now.strftime("%d_%m_%Y %H_%M_%S")
    df.to_csv(f'{output_path}output - {dtt}.csv', index=False)

    print("Finished")


if __name__ == "__main__":
    
    transform_csv(
        # A changer selon le nom du fichier à transformer
        source_file_path='data/2023_07_15 new.csv',

        # Dossier de sortie
        output_path="output/"
    )