import datetime


def generate_output_filename():
    now = datetime.datetime.now()
    return (
        "20230403_060023_dacobello_blockchain - "
        + now.strftime("%d_%m_%Y %H_%M_%S")
        + ".csv"
    )
