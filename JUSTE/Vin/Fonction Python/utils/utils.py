import datetime


def generate_output_filename():
    now = datetime.datetime.now()
    return (
        "20220729 - Fichier traçabilité mise en bouteille du - "
        + now.strftime("%d_%m_%Y %H_%M_%S")
        + ".csv"
    )
