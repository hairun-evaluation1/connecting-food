import os
import pandas as pd
from model.contribution import Contribution
from utils.constant import (
    INPUT_FILE_PATH,
    OUTPUT_FILE_PATH,
    NULL,
    STEP,
    CATEGORY,
)

SOURCE_PATH = os.path.dirname(os.path.abspath(__file__))


def format_csv(source_file_path, output_file_path):
    """Function used to transform an input csv into a formatted one"""

    df = pd.read_csv(source_file_path, delimiter=";")
    column_number = df.shape[1]
    row_list = []

    # Check if column number of the input data exceed 12
    if column_number >= 12:
        print("Formatting...")

        # Get input data line by line
        for index, row in df.iterrows():

            # Dict model
            contribution_dict = {
                "category": CATEGORY,
                "step": STEP,
                "level": NULL,
                "capture_start": row.get("Date"),
                "capture_end": NULL,
                "capture_uid": NULL,
                "sender_actor_number": row.get("Code adhérent"),
                "sender_location_number": row.get("Code adhérent"),
                "sender_asset_number": NULL,
                "sender_track_id": NULL,
                "sender_sku": NULL,
                "sender_batch_number": NULL,
                "sender_model_number": NULL,
                "sender_production_date": row.get("Date"),
                "sender_product": row.get("Code produit"),
                "sender_quantity": row.get("Poids Net"),
                "sender_unit": NULL,
                "receiver_actor_number": NULL,
                "receiver_location_number": row.get("Code acteur dépôt"),
                "receiver_asset_number": row.get("Code acteur dépôt"),
                "receiver_track_id": row.get("Code acteur dépôt"),
                "receiver_sku": NULL,
                "receiver_batch_number": NULL,
                "receiver_model_number": NULL,
                "receiver_production_date": NULL,
                "receiver_product": NULL,
                "receiver_quantity": NULL,
                "receiver_unit": NULL,
                "tags": NULL,
                "payload__année_de_récolte": row.get("Année de récolte"),
                "payload__date_de_récolte": row.get("Date de récolte"),
                "payload__type_de_transfert": row.get("Type de transfert"),
            }

            row_list.append(Contribution(**contribution_dict).__dict__)

        # Dataframe transformation
        result = pd.DataFrame(row_list)
        # Dataframe into CSV
        result.to_csv(output_file_path, index=False, sep=",")
        print("Finished")
    else:
        print("There is not enough column")


if __name__ == "__main__":

    format_csv(
        source_file_path=SOURCE_PATH + INPUT_FILE_PATH,
        output_file_path=SOURCE_PATH + OUTPUT_FILE_PATH,
    )
