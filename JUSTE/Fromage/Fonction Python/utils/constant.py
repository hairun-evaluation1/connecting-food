from utils.utils import generate_output_filename

NULL = None
PRODUCT = "Fromage Juste"
CATEGORY = "Fromage"
STEP = "processing"
INPUT_FILE_PATH = "/data/20210909 - FROMAGE JUSTE Tracabilité Collecte du 20210706.csv"
OUTPUT_FILE_PATH = "/output/Output - " + generate_output_filename()
