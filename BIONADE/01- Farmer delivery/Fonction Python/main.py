import os
import pandas as pd
from model.farmer_delivery import FarmerDelivery
from utils.constant import (
    INPUT_FILE_PATH,
    OUTPUT_FILE_PATH,
    NULL,
    STEP,
    CATEGORY,
    SITE_SENDER,
    SITE_RECEIVER,
    ACTOR,
)

SOURCE_PATH = os.path.dirname(os.path.abspath(__file__))


def format_csv(source_file_path, output_file_path):
    """Function used to transform an input csv into a formatted one"""

    df = pd.read_csv(source_file_path, delimiter=";")
    column_number = df.shape[1]
    row_list = []

    # Check if column number of the input data exceed 17
    if column_number >= 17:
        print("Formatting...")

        # Get input data line by line
        for index, row in df.iterrows():
            flow_dates = (
                row.get("Farmer delivery date").replace(" ", "").split(",")
            )
            for flow_date in flow_dates:
                # Dict model
                farmer_delivery_dict = {
                    "category": CATEGORY,
                    "step": STEP,
                    "level": NULL,
                    "capture_start": flow_date.replace(".19", ".2019"),
                    "capture_end": NULL,
                    "capture_uid": NULL,
                    "sender_actor_number": row.get("Farmer internal code"),
                    "sender_location_number": SITE_SENDER,
                    "sender_asset_number": NULL,
                    "sender_track_id": NULL,
                    "sender_sku": NULL,
                    "sender_batch_number": NULL,
                    "sender_model_number": NULL, 
                    "sender_production_date": flow_date,
                    "sender_product": row.get("Product code in"),
                    "sender_quantity": row.get(
                        "Quantity total delivered by the farmer (tons)"
                    ).replace(",","."),
                    "sender_unit": NULL,
                    "receiver_actor_number": ACTOR,
                    "receiver_location_number": SITE_RECEIVER,
                    "receiver_asset_number": NULL,
                    "receiver_track_id": f'{row.get("Concentrate batch number")}{row.get("Year of production")}',
                    "receiver_sku": NULL,
                    "receiver_batch_number": NULL,
                    "receiver_model_number": NULL,
                    "receiver_production_date": NULL,
                    "receiver_product": NULL,
                    "receiver_quantity": NULL,
                    "receiver_unit": NULL,
                    "tags": NULL,
                    "payload__year_of_production": row.get(
                        "Year of production"
                    ),
                    "payload__harvest_start_date": row.get(
                        "Harvest start date"
                    ),
                    "payload__harvest_end_date": row.get("Harvest end date"),
                    "payload__total_quantity_delivered": row.get(
                        "Quantity total delivered by the farmer (tons)"
                    ).replace(",","."),
                    "payload__concentrate_batch_number": row.get(
                        "Concentrate batch number"
                    ),
                }

                row_list.append(
                    FarmerDelivery(**farmer_delivery_dict).__dict__
                )

        # Dataframe transformation
        result = pd.DataFrame(row_list)
        # Dataframe into CSV
        result.to_csv(output_file_path, index=False, sep=",")
        print("Finished")
    else:
        print("There is not enough column")


if __name__ == "__main__":

    format_csv(
        source_file_path=SOURCE_PATH + INPUT_FILE_PATH,
        output_file_path=SOURCE_PATH + OUTPUT_FILE_PATH,
    )
