import datetime


def generate_output_filename():
    now = datetime.datetime.now()
    return (
        "20230405 - Traçabilite JUST ET VENDEEN - "
        + now.strftime("%d_%m_%Y %H_%M_%S")
        + ".csv"
    )
